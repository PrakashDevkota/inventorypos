﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace IMS.Service.Models
{
   public class ProductDetailsModel
    {
        public int Id { get; set; }
        public int Propduct_id { get; set; }
        public string Specification_Name { get; set; }
        public string Details { get; set; }
        public string  Product_name { get; set; }

        public int Office_Id { get; set; }

        public List<SelectListItem> ProductList { get; set; }

        public List<ProductModel> ProductModelList { get; set; }


    }
}
