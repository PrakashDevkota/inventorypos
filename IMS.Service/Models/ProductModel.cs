﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMS.Service.Models
{
   public class ProductModel
    {
        public int Id { get; set; }

        public string Product_name { get; set; }

       

        public int Saman_barga_id { get; set; }

        public string Saman_Barga_Name { get; set; }

        public int Saman_prakriti_id { get; set; }

        public int Unit_Id { get; set; }

        public int Office_Id { get; set; }

        public string Slug { get; set; }

        public int Status { get; set; }

        public int Created_by { get; set; }

        public DateTime Created_date { get; set; }

        public string Category_name { get; set; }

        public string Saman_kisim_name { get; set; }

      

        public int Category_Id { get; set; }

        public List<ProductModel> ProductModels { get; set; }
    }

   
}
