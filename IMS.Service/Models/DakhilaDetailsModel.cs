﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMS.Service.Models
{
  public class DakhilaDetailsModel
    {
        public int Id { get; set; }
        public int dakhila_id { get; set; }
        public int Kharid_adesh_id { get; set; }
        public int Category_Id { get; set; }
        public int Product_id { get; set; }
        public int Product_Details_Id { get; set; }
        public int Kharid_type_id { get; set; }
        public int Quantity { get; set; }
        public int Seller_id { get; set; }
    }
}
