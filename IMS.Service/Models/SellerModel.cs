﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMS.Service.Models
{
   public class SellerModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Phone { get; set; }

        public string Mobile { get; set; }

        public int Office_Id { get; set; }

        public string Darta_number { get; set; }

        public string Owner_name { get; set; }

        public string Address { get; set; }

        public int Status { get; set; }

        public int Created_By { get; set; }

        public string Created_Date { get; set; }
    }
}
