﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

 namespace IMS.Service.Models
{
   public class DakhilaModel
    {
        public int Id { get; set; }

        public int Kharid_adesh_id { get; set; }      

        public int Status { get; set; }

        public int Created_By { get; set; }

        public String Created_date { get; set; }

        public string Office_id{ get; set; }

        public string Fiscal_year { get; set; }

        public List<SellerModel> sellerModelList { get; set; }




    }
}
