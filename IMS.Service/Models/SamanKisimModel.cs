﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace IMS.Service.Models
{
   public class SamanKisimModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Saman_barga_id { get; set; }
        public int Status { get; set; }
        public int Created_by { get; set; }
        public string Created_date { get; set; }

       public List<SelectListItem> SamanBargaList { get; set; }

    }
}
