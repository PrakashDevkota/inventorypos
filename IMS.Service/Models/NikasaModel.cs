﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMS.Service.Models
{
   public class NikasaModel
    {
        public int Id { get; set; }
        public int Nikasa_Number { get; set; }
        public int Mag_Faram_Id { get; set; }
        public int Office_Id { get; set; }
        public int Request_By { get; set; }
        public int Created_By { get; set; }
        public int Status { get; set; }
        public string Created_Date { get; set; }
    }
}
