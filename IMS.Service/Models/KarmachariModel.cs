﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMS.Service.Models
{
   public class KarmachariModel
    {
        public int Id { get; set; }
       
        public int Office_Id { get; set; }  
        public int Status { get; set; }
        public DateTime Created_date { get; set; }
        public int Created_by { get; set; }
    }
}
