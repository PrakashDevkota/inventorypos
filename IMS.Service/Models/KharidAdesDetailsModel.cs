﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace IMS.Service.Models
{
   public class KharidAdesDetailsModel
    {
        public int Id { get; set; }

        [Required]
        public int Kharid_ades_id  { get; set; }

        public int Category_Id { get; set; }

        public string Category_Name { get; set; }

        [Required]
        public int Product_id { get; set; }

        public int Product_Details_Id { get; set; }


        [Required]
        public int Kharid_type_id { get; set; }

        [Required]
        public int Quantity { get; set; }

        public byte Status { get; set; }

        public KharidAdeshModel KharidAdeshModel { get; set; }

        public int Kharid_Adesh_Number { get; set; }

        public List<SelectListItem> KharidAdeshListItem { get; set; }

        public List<KharidAdesDetailsModel> kharidAdeshModelList { get; set; }

        public string Created_Date { get; set; }

        public string Product_Name { get; set; }

        public string Product_Specification { get; set; }

        public string Kharid_Type_Name { get; set; }




        public string Details { get; set; }

        public string Unit_Name { get; set; }

    }
}
