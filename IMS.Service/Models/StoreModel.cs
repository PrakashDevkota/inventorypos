﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMS.Service.Models
{
   public class StoreModel
    {
        public int Id { get; set; }

        public int Product_Id { get; set; }

        public int Quantity { get; set; }

        public int Unit_Id { get; set; }

        public int Office_Id { get; set; }

        public int Created_By { get; set; }

        public string Created_Date { get; set; }

    }
}
