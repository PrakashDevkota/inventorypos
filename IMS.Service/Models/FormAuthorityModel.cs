﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace IMS.Service.Models
{
   public class FormAuthorityModel
    {
        public int Id { get; set; }

        public int Ready_By { get; set; }

        public int Submit_By { get; set; }

        public int Approved_By { get; set; }

        public int Karmchari_Id { get; set; }

        public List<SelectListItem> KarmachariList { get; set; }

        public int Office_Id { get; set; }



    }
}
