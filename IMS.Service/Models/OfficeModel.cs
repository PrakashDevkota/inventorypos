﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMS.Service.Models
{
   public class OfficeModel
    {
        public int Id { get; set; }
        public int DistrictId { get; set; }
        public string LocalLevelCode { get; set; }
        public string NameEnglish { get; set; }
        public string NameNepali { get; set; }
        public string ProjectName { get; set; }
        public int Status { get; set; }

        public string District_Name { get; set; }

        public string Local_Level_Type_Nmae { get; set; }

    }
}
