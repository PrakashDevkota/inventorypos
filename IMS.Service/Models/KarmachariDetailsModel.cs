﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace IMS.Service.Models
{
   public class KarmachariDetailsModel
    {
        public int Id { get; set; }

        public int Karmachari_id { get; set; }

        public string First_Name { get; set; }

        public string  Last_Name { get; set; }

        public string Sewa { get; set; }

        public string Samuha { get; set; }

        public string Pad { get; set; }

        public int  Office_Id { get; set; }

        public List<SelectListItem> KarmachariList { get; set; }

        public int Ready_By { get; set; }

        public int Submit_By { get; set; }

        public int Approvd_By { get; set; }



    }
}
