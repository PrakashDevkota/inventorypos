﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMS.Service.Models
{
   public class MagfaramModel
    {
        public int Id { get; set; }
        public int Mag_faram_number { get; set; }
        
        public int Office_id { get; set; }
        public string Created_date { get; set; }
        public int Created_by { get; set; }
        public int Approved_by { get; set; }
        public int Fiscal_year { get; set; }
    }
}

