﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMS.Service.Models
{
   public class KharidAdeshModel
    {
        public Int32 Id { get; set; }

      
        public int KharidAdesNumber { get; set; }
      
        public Int32 product_id { get; set; }
       
        //public DateTime date { get; set; }

        public int Status { get; set; }

        public string CreatedDate { get; set; }

        public string Created_by { get; set; }

        public String Fiscal_year { get; set; }

        public string Office_id { get; set; }

        public string Product_name { get; set; }

        public int Category_Id { get; set; }

        public int Product_Details_Id { get; set; }

        public List<KharidAdeshModel> KharidAdeshModels { get; set; }

    }
}
