﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace IMS.Service.Models
{
   public class MagFaramDetailsModel
    {
        public int Id { get; set; }
        public int Mag_Faram_Id { get; set; }
        public int Request_By { get; set; }
        public int Product_Id { get; set; }
        public int Quantity { get; set; }


        public int Category_Id { get; set; }

        public int Product_Details_Id { get; set; }

        


        public List<SelectListItem> MagFaramList { get; set; }

        public string Created_Date { get; set; }

        public int Mag_Faram_Number { get; set; }

        public string Category_Name { get; set; }

        public string Product_Name { get; set; }

        public string Product_Specification { get; set; }

        public string Kharid_Type { get; set; }

        public string KarmaChari { get; set; }

    }
}
