﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;


namespace IMS.Service.Models
{
   public partial class UserModel
    {
        
        public int Id { get; set; }

       
        public string Username { get; set; }

       
        public string Password { get; set; }

        public int Status { get; set; }
    }
}
