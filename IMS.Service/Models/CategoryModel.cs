﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace IMS.Service.Models
{
   public class CategoryModel
    {
        public int Id { get; set; }
        [Required]
        public string Category_name { get; set; }

        [Required]
        public int Parent_id { get; set; }
        
       
    }
}
