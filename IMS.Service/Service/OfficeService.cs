﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IMS.Service.Inteface;
using IMS.Repository.Entities;
using IMS.Service.Models;
using System.Data.SqlClient;


namespace IMS.Service.Service
{
    public class OfficeService : IOfficeService
    {
        public OfficeModel GetOfficeByUser(int id)
        {
            //List<OfiiceModel> lstOffice = new List<OfiiceModel>();
            SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-GAFSDI8\SQLEXPRESS; Integrated Security=True; Database=llims");
            string sql = @"SELECT o.project_name, o.id,o.local_level_code,o.local_level_name_nep,o.district_id,d.district_name_nep,o.local_level_type_id,llt.name
                        FROM users u
                        INNER JOIN user_karmachari uk
                        ON u.id = uk.users_id

                        INNER JOIN karmachari k
                        ON uk.karmachari_id = k.id

                        INNER JOIN office o
                        ON k.office_id = o.id

                        INNER JOIN district d
                        ON d.id = o.district_id

                        INNER JOIN local_level_type llt
                        ON llt.id = o.local_level_type_id

                        where u.id ="+ id.ToString();
            SqlCommand cmd = new SqlCommand(sql, con);
            //cmd.Parameters.AddWithValue("@users.id", id);
            SqlDataReader dr = null;
            con.Open();
            dr = cmd.ExecuteReader();
            OfficeModel cd = new OfficeModel();
            if (dr.Read())
            {


                cd.NameNepali = dr["local_level_name_nep"].ToString();
                cd.ProjectName = dr["project_name"].ToString();
                cd.Id = Convert.ToInt32(dr["id"]);
                cd.DistrictId = Convert.ToInt32(dr["district_id"]);
                cd.District_Name = dr["district_name_nep"].ToString();
                cd.Local_Level_Type_Nmae = dr["name"].ToString();
                cd.LocalLevelCode = dr["local_level_code"].ToString();


            }
            dr.Close();
            con.Close();
            return cd;
        }
    }
}
