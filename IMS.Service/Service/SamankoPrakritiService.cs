﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using IMS.Repository.Entities;
using IMS.Service.Inteface;
using IMS.Service.Models;

namespace IMS.Service.Service
{
    public class SamankoPrakritiService : ISamankoPrakritiService
    {
        //hello
        private llimsEntities db;
        public SamankoPrakritiService()
        {
            this.db = new llimsEntities();
        }

        public List<SamankoPrakritiModel> GetAll()
        {
            List<saman_prakriti> Record = db.saman_prakriti.ToList();
            List<SamankoPrakritiModel> ModelRecord = new List<SamankoPrakritiModel>();
            foreach (var item in Record)
            {// you can map entity to model here
                ModelRecord.Add(new SamankoPrakritiModel
                {
                    Id = item.id,
                    Name = item.name,
                    // Status = item.status

                });
            }
            return ModelRecord;
        }

        public List<SelectListItem> GetSamanPrakritiSelectListItem()
        {
            List<saman_prakriti> Record = db.saman_prakriti.ToList();
            List<SelectListItem> modelRecord = new List<SelectListItem>();
            foreach (var item in Record)
            {
                SelectListItem single = new SelectListItem
                {
                    Value = item.id.ToString(),
                    Text = item.name
                };
                modelRecord.Add(single);

            };
            return modelRecord;
        }

        public int Insert(SamankoPrakritiModel obj)
        {
            saman_prakriti tbl = new saman_prakriti();
            tbl.name = obj.Name;
            db.saman_prakriti.Add(tbl);
            return db.SaveChanges();
        }

        public int Save()
        {
            return db.SaveChanges();
        }

        public int SelectByID(int id)
        {
            throw new NotImplementedException();
        }

        public int Update(SamankoPrakritiModel obj)
        {
            saman_prakriti tbl = new saman_prakriti();
            tbl = db.saman_prakriti.Where(u => u.id == obj.Id).FirstOrDefault();
            tbl.name = obj.Name;
            return db.SaveChanges();

        }

        public int Delete(int id)
        {
            saman_prakriti samanPrakriti = db.saman_prakriti.Where(u => u.id == id).FirstOrDefault();
            db.saman_prakriti.Remove(samanPrakriti);
            return db.SaveChanges();
        }
    }
}
