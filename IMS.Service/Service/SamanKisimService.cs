﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IMS.Service.Inteface;
using IMS.Service.Models;
using IMS.Repository.Entities;
using System.Web.Mvc;

namespace IMS.Service.Service
{
    public class SamanKisimService : ISamanKisimService
    {

        private llimsEntities db;
        public SamanKisimService()
        {
            this.db = new llimsEntities();
        }

        public List<SamanKisimModel> GetAll()
        {
            List<saman_kisim> entityRecord = db.saman_kisim.ToList();
            List<SamanKisimModel> modelRecord = new List<SamanKisimModel>();
            foreach (var item in entityRecord)
            {
                modelRecord.Add(new SamanKisimModel {
                    Id = item.id,
                    Name = item.saman_kisim_name,
                    Saman_barga_id = Convert.ToInt32(item.saman_barga_id),
                    Status = Convert.ToInt32(item.status),


                });
            } return modelRecord;
        }

        public List<SelectListItem> GetSamanKisimSelectListItem()
        {
            List<saman_kisim> Record = db.saman_kisim.ToList();
            List<SelectListItem> modelRecord = new List<SelectListItem>();
            foreach (var item in Record)
            {
                SelectListItem single = new SelectListItem
                {
                    Value = item.id.ToString(),
                    Text = item.saman_kisim_name
                };
                modelRecord.Add(single);

            };
            return modelRecord;
        }
        public List<SamanKisimModel> GetAllSamanKisim()
        {
            throw new NotImplementedException();
        }

        public int Insert(SamanKisimModel obj)
        {
            saman_kisim entityObj = new saman_kisim();
            entityObj.saman_kisim_name = obj.Name;
            entityObj.saman_barga_id = obj.Saman_barga_id;
            entityObj.status = 1;
            entityObj.created_by = null;
            //entityObj.created_date = obj.Created_date;
            db.saman_kisim.Add(entityObj);
            return db.SaveChanges();
        }

        public int Save()
        {
            return db.SaveChanges();
        }

        public int SelectByID(int id)
        {
            throw new NotImplementedException();
        }

        public int Update(SamanKisimModel obj)
        {
            saman_kisim tbl = new saman_kisim();
            tbl = db.saman_kisim.Where(u => u.id == obj.Id).FirstOrDefault();
            // tbl.id = obj.Id;
            tbl.saman_kisim_name = obj.Name;
            tbl.saman_barga_id = obj.Saman_barga_id;
            tbl.status = obj.Status;

            return db.SaveChanges();
        }

        public int Delete(int id)
        {
            saman_kisim tbl = db.saman_kisim.Where(u => u.id == id).FirstOrDefault();
            db.saman_kisim.Remove(tbl);
            return db.SaveChanges();
        }
    }
}
