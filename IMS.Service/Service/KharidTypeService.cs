﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using IMS.Repository.Entities;
using IMS.Service.Inteface;
using IMS.Service.Models;

namespace IMS.Service.Service
{
    public class KharidTypeService : IKharidTypeService
    {

        private llimsEntities db;

        public KharidTypeService()
        {
            this.db = new llimsEntities();
        }

        public List<KharidTypeModel> GetAll()
        {
            List<kharid_prakar> Record = db.kharid_prakar.ToList();
            List<KharidTypeModel> ModelRecord = new List<KharidTypeModel>();
            foreach (var item in Record)
            {// you can map entity to model here
                ModelRecord.Add(new KharidTypeModel
                {
                    Id = item.id,
                    Name = item.name,
                   
                });
            }
            return ModelRecord;
        }

        public List<SelectListItem> GetKharidPrakarSelectListItem()
        {
            List<kharid_prakar> Record = db.kharid_prakar.ToList();
            List<SelectListItem> modelRecord = new List<SelectListItem>();
            foreach (var item in Record)
            {
                SelectListItem single = new SelectListItem
                {
                    Value = item.id.ToString(),
                    Text = item.name,
                };
                modelRecord.Add(single);

            };
            return modelRecord;
        }

        public int Insert(KharidTypeModel obj)
        {
            kharid_prakar tbl = new kharid_prakar();
            tbl.name = obj.Name;
            

            db.kharid_prakar.Add(tbl);
            return db.SaveChanges();
        }

        public int Save()
        {
            return db.SaveChanges();

        }

       
        public int Update(KharidTypeModel obj)
        {
            kharid_prakar tbl = new kharid_prakar();
            tbl = db.kharid_prakar.Where(u => u.id == obj.Id).FirstOrDefault();
            // tbl.id = obj.Id;
            tbl.name = obj.Name;
           
            return db.SaveChanges();
        }
        public int Delete(int id)
        {
            kharid_prakar tbl = db.kharid_prakar.Where(u => u.id == id).FirstOrDefault();
            db.kharid_prakar.Remove(tbl);
            return db.SaveChanges();
        }

        public int SelectByID(int id)
        {
            throw new NotImplementedException();
        }

    }
}
