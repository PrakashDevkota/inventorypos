﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IMS.Service.Inteface;
using IMS.Repository.Entities;
using IMS.Service.Models;
using System.Web.Mvc;

namespace IMS.Service.Service
{
    public class ProductDetailsService : IProductDetailsService
    {
        private llimsEntities db = null;
        public ProductDetailsService()
        {
            this.db = new llimsEntities();
        }

        public List<ProductDetailsModel> GetAll()
        {
            //List<product_details> EntityRecord = db.product_details.ToList();
            List<ProductDetailsModel> ModelRecord = new List<ProductDetailsModel>();
            var EntityRecord = (from p in db.products
                          from pd in db.product_details
                          where p.id == pd.product_id

                          select new
                          {
                             product_name = p.product_name,
                             name = pd.specification_name,
                             details = pd.details

                          }).ToList();

            foreach (var item in EntityRecord)
            {
                ModelRecord.Add(new ProductDetailsModel
                {
                    //Propduct_id = Convert.ToInt32(item.product_id),
                    Product_name = item.product_name,
                    Specification_Name = item.name,
                    Details = item.details

                });
            }
            return ModelRecord;
        }

        public List<SelectListItem> GetProductDetailsList()
        {
            List<product_details> Record = db.product_details.ToList();
            List<SelectListItem> modelRecord = new List<SelectListItem>();
            foreach (var item in Record)
            {
                SelectListItem single = new SelectListItem
                {
                    Value = item.id.ToString(),
                    Text = item.specification_name,
                };
                modelRecord.Add(single);

            };
            return modelRecord;
        }

        public int Insert(ProductDetailsModel obj)
        {
            product_details EntityRecord = new product_details();
            EntityRecord.product_id = obj.Propduct_id;
            EntityRecord.specification_name = obj.Specification_Name;
            EntityRecord.details = obj.Details;
            EntityRecord.office_id = obj.Office_Id;
            db.product_details.Add(EntityRecord);
            return db.SaveChanges();
        }

        public int Save()
        {
            return db.SaveChanges();
        }

        public int Update(ProductDetailsModel obj)
        {
            product_details entityRecord = db.product_details.Where(p => p.id.Equals(obj.Id)).FirstOrDefault();
            entityRecord.product_id = obj.Propduct_id;
            entityRecord.specification_name = obj.Specification_Name;
            entityRecord.details = obj.Details;
            return db.SaveChanges();

        }

        public int Delete(int id)
        {
            var entityRecord = db.product_details.Where(p => p.id.Equals(id)).FirstOrDefault();
            db.product_details.Remove(entityRecord);
            return db.SaveChanges();
        }

        public int SelectById(int id)
        {
            throw new NotImplementedException();
        }

        public List<ProductDetailsModel> getProductDetailsByOfficeIdAndProductId(int office_id, int product_id)
        {
            List<ProductDetailsModel> productDetailsList = new List<ProductDetailsModel>();
            List<product_details> record = db.product_details.Where(p => p.office_id == office_id && p.product_id == product_id).ToList();
            foreach (var item in record)
            {
                productDetailsList.Add(new ProductDetailsModel
                {
                    Id = item.id,
                    Specification_Name = item.specification_name,
                });
            }
            return productDetailsList;
        }
    }
}
