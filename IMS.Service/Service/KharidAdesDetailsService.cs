﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IMS.Service.Inteface;
using IMS.Service.Models;
using IMS.Repository.Entities;
using System.Web.Mvc;
using System.Data.SqlClient;

namespace IMS.Service.Service
{
    public class KharidAdesDetailsService : IKharidAdesDetailsService
    {


        private llimsEntities db = null;
        public KharidAdesDetailsService()
        {
            this.db = new llimsEntities();
        }


        public List<KharidAdesDetailsModel> GetAll()
        {
            List<kharid_aadesh_details> entityrecord = db.kharid_aadesh_details.ToList();
            List<KharidAdesDetailsModel> modelRecord = new List<KharidAdesDetailsModel>();
            foreach (var item in entityrecord)
            {
                modelRecord.Add(new KharidAdesDetailsModel
                {
                    Id = item.id,
                    Kharid_ades_id = Convert.ToInt32(item.kharid_aadesh_id),
                    Product_id = Convert.ToInt32(item.product_id),
                    Kharid_type_id = Convert.ToInt32(item.kharid_prakar_id),
                    Quantity = Convert.ToInt32(item.quantity),

                });
            }

            return modelRecord;

        }


        public int Insert(KharidAdesDetailsModel obj)
        {
            kharid_aadesh_details tbl = new kharid_aadesh_details();
            tbl.kharid_aadesh_id = obj.Kharid_ades_id;
            tbl.category_id = obj.Category_Id;
            tbl.product_id = obj.Product_id;
            tbl.kharid_prakar_id = obj.Kharid_type_id;
            tbl.product_details_id = obj.Product_Details_Id;
            tbl.quantity = obj.Quantity;

            db.kharid_aadesh_details.Add(tbl);
            return db.SaveChanges();
        }

        public int Save()
        {
            return db.SaveChanges();
        }

        public int SelectByID(int id)
        {
            throw new NotImplementedException();
        }

        //public List<KharidAdesDetailsModel> GetByKharidAdeshId(int id)
        //{
        //    List<kharid_aadesh_details> kharidAdeshDetails = db.kharid_aadesh_details.Where(x => x.kharid_aadesh_id == id).ToList();
        //    List<KharidAdesDetailsModel> ModelRecord = new List<KharidAdesDetailsModel>();
        //    SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-A6MRTL9\SQLEXPRESS; Integrated Security=True; Database=llims");
        //    foreach (var item in kharidAdeshDetails)
        //    {
        //        ModelRecord.Add(new KharidAdesDetailsModel
        //        {
        //            Id = item.id,
        //            Kharid_ades_id = item.id,
        //            Category_Id = Convert.ToInt32(item.category_id),
        //            Kharid_type_id = Convert.ToInt32(item.kharid_prakar_id),
        //            Product_id = Convert.ToInt32(item.product_id),
        //            Product_Details_Id = Convert.ToInt32(item.product_details_id),
        //            Quantity = Convert.ToInt32(item.quantity),
        //        });
        //    }

        //    return ModelRecord;
        //   }

        public List<KharidAdesDetailsModel> GetByKharidAdeshId(int id)
        {
            List<KharidAdesDetailsModel> ModelRecord = new List<KharidAdesDetailsModel>();
            SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-GAFSDI8\SQLEXPRESS; Integrated Security=True; Database=llims");
            string sql = @"select kd.quantity,k.office_id,c.category_name,p.product_name,
	                        pd.specification_name, kk.name
                        from kharid_aadesh_details kd
                        INNER JOIN kharid_aadesh k
                        ON kd.kharid_aadesh_id = k.id

                        INNER JOIN category c
                        ON kd.category_id = c.id

                        INNER JOIN product p
                        ON kd.product_id = p.id

                        INNER JOIN product_details pd
                        ON kd.product_details_id = pd.id

                        INNER JOIN kharid_prakar kk
                        ON kd.kharid_prakar_id = kk.id

                        where kd.kharid_aadesh_id =" + id.ToString();
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataReader dr = null;
            con.Open();
            dr = cmd.ExecuteReader();
            while(dr.Read())
            {
                KharidAdesDetailsModel cd = new KharidAdesDetailsModel();
                cd.Quantity = Convert.ToInt32(dr["quantity"]);
                cd.Category_Name = dr["category_name"].ToString();
                cd.Product_Name = dr["product_name"].ToString();
                cd.Product_Specification = dr["specification_name"].ToString();
                cd.Kharid_Type_Name = dr["name"].ToString();
                ModelRecord.Add(cd);
            }
            dr.Close();
            con.Close();
            return ModelRecord;
        }

        public List<KharidAdesDetailsModel> GetKharidAdeshDetailsByKharidAdeshId(int id)
        {
            List<KharidAdesDetailsModel> lstKharidAdesh = new List<KharidAdesDetailsModel>();
            SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-GAFSDI8\SQLEXPRESS; Integrated Security=True; Database=llims");
            string sql = @"select kd.product_id, kd.kharid_prakar_id,kd.quantity, k.kharid_ades_number,k.created_date,p.product_name,pd.specification_name,pd.details,u.unit_name
                    from kharid_aadesh_details kd
                    INNER JOIN kharid_aadesh k
                    ON kd.kharid_aadesh_id = k.id 
                    INNER JOIN product p
                    ON p.id = kd.product_id
                    INNER JOIN product_details pd
					ON pd.product_id = p.id
                    
                    INNER JOIN units u
                    ON p.unit_id = u.id
                        where kd.kharid_aadesh_id =" + id.ToString();
            SqlCommand cmd = new SqlCommand(sql, con);
            //cmd.Parameters.AddWithValue("@users.id", id);
            SqlDataReader dr = null;
            con.Open();
            dr = cmd.ExecuteReader();
           
            while (dr.Read())
            {
                KharidAdesDetailsModel cd = new KharidAdesDetailsModel();
                cd.Product_id = Convert.ToInt32(dr["product_id"]);
                cd.Kharid_Adesh_Number = Convert.ToInt32(dr["kharid_ades_number"]);
                cd.Kharid_type_id = Convert.ToInt32(dr["kharid_prakar_id"]);
                cd.Quantity = Convert.ToInt32(dr["quantity"]);
                cd.Created_Date = dr["created_date"].ToString();
                cd.Product_Name = dr["product_name"].ToString();
                cd.Product_Specification = dr["specification_name"].ToString();
                cd.Details = dr["details"].ToString();
                cd.Unit_Name = dr["unit_name"].ToString();
                lstKharidAdesh.Add(cd);

            }
            dr.Close();
            con.Close();
            return lstKharidAdesh;
            
        }

        public List<KharidAdesDetailsModel> getKharidAdeshDetailsByKharidAdeshIdAndOfficeId(int khridAdesh_id, int office_id)
        {
            List<KharidAdesDetailsModel> data = new List<KharidAdesDetailsModel>();
            SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-GAFSDI8\SQLEXPRESS; Integrated Security=True; Database=llims");
            string sql = @"select
                                kd.product_id,
                                kd.id,
                                kd.kharid_prakar_id,
                                kd.quantity,
                                k.office_id,
                                kd.product_details_id,
                                kd.kharid_aadesh_id
                                
                        from kharid_aadesh_details  kd
                        INNER JOIN kharid_aadesh k
                        ON kd.kharid_aadesh_id = k.id
                        where kd.kharid_aadesh_id =" + khridAdesh_id + " and k.office_id=" + office_id.ToString();
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataReader dr = null;
            con.Open();
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                KharidAdesDetailsModel cd = new KharidAdesDetailsModel();
                cd.Kharid_ades_id = Convert.ToInt32(dr["kharid_aadesh_id"]);
                cd.Product_id = Convert.ToInt32(dr["product_id"]);
                cd.Kharid_type_id = Convert.ToInt32(dr["kharid_prakar_id"]);
                cd.Quantity = Convert.ToInt32(dr["quantity"]);
                cd.Product_Details_Id = Convert.ToInt32(dr["product_details_id"]);
                cd.Id = Convert.ToInt32(dr["id"]);
                
                data.Add(cd);

            }
            dr.Close();
            con.Close();
            return data;

        }

        //public List<KharidAdeshModel> ffgetKharidAdeshDetailsByOfficeId(int office_id)
        //{
        //    //List<kharid_aadesh> entity = db.kharid_aadesh.Where(o => o.office_id == office_id).ToList();
        //    List<KharidAdesDetailsModel> model = new List<KharidAdesDetailsModel>();
        //    SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-A6MRTL9\SQLEXPRESS; Integrated Security=True; Database=llims");
        //    string sql = @"select k.kharid_ades_number,kd.id,k.created_date
        //                from kharid_aadesh_details kd
        //                INNER JOIN kharid_aadesh k
        //            ON Kd.kharid_aadesh_id = k.id

        //            where k.office_id =" + office_id.ToString();
        //    SqlCommand cmd = new SqlCommand(sql, con);
        //    SqlDataReader dr = null;
        //    con.Open();
        //    dr = cmd.ExecuteReader();
        //    while (dr.Read())
        //    {
        //        KharidAdesDetailsModel cd = new KharidAdesDetailsModel();
        //        cd.Kharid_Adesh_Number = Convert.ToInt32(dr["kharid_ades_number"]);
        //        cd.Id = Convert.ToInt32(dr["id"]);
        //        cd.Created_Date = dr["created_date"].ToString();
        //        model.Add(cd);
        //    }
        //    dr.Close();
        //    con.Close();
        //    return model;
        //}



        public int Update(KharidAdesDetailsModel obj)
        {
            kharid_aadesh_details tbl = new kharid_aadesh_details();
            tbl = db.kharid_aadesh_details.Where(x => x.id == obj.Id).FirstOrDefault();

            // field update here

            return db.SaveChanges();

        }

        public int Delete(int id)
        {
            kharid_aadesh_details tbl = new kharid_aadesh_details();
            tbl = db.kharid_aadesh_details.Where(x => x.id == id).FirstOrDefault();
            db.kharid_aadesh_details.Remove(tbl);
            return db.SaveChanges();
        }

        public List<SelectListItem> GetKharidAdesDetailsList()
        {
            List<kharid_aadesh_details> Record = db.kharid_aadesh_details.ToList();
            List<SelectListItem> modelRecord = new List<SelectListItem>();
            foreach (var item in Record)
            {
                SelectListItem single = new SelectListItem
                {
                    Value = item.id.ToString(),
                    Text = item.kharid_aadesh_id.ToString(),
                   
                    //Text = item.product_id.ToString()
                  
                };
                modelRecord.Add(single);

            };
            return modelRecord;
        }

        public  List<KharidAdesDetailsModel> getKharidAdeshDetailsByOfficeId(int id)
        {
            //List<kharid_aadesh> entity = db.kharid_aadesh.Where(o => o.office_id == office_id).ToList();
            List<KharidAdesDetailsModel> model = new List<KharidAdesDetailsModel>();
            SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-GAFSDI8\SQLEXPRESS; Integrated Security=True; Database=llims");
            string sql = @"select k.kharid_ades_number,kd.id,k.created_date
                        from kharid_aadesh_details kd
                        INNER JOIN kharid_aadesh k
                    ON Kd.kharid_aadesh_id = k.id

                    where k.office_id =" + id.ToString();
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataReader dr = null;
            con.Open();
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                KharidAdesDetailsModel cd = new KharidAdesDetailsModel();
                cd.Kharid_Adesh_Number = Convert.ToInt32(dr["kharid_ades_number"]);
                cd.Id = Convert.ToInt32(dr["id"]);
                cd.Created_Date = dr["created_date"].ToString();
                model.Add(cd);
            }
            dr.Close();
            con.Close();
            return model;
        }
    }
}
