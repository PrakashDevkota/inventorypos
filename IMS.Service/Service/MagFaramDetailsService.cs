﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IMS.Service.Models;
using IMS.Repository.Entities;
using IMS.Service.Inteface;
using System.Data.SqlClient;

namespace IMS.Service.Service
{
    public class MagFaramDetailsService : IMagFaramDetailsService
    {
        llimsEntities db;
        public MagFaramDetailsService()
        {
            this.db = new llimsEntities();
        }
        public List<MagFaramDetailsModel> GetAll()
        {
            throw new NotImplementedException();
        }

        public int Insert(MagFaramDetailsModel obj)
        {
            mag_faram_details tbl = new mag_faram_details();
            tbl.mag_faram_id = obj.Mag_Faram_Id;
            tbl.category_id = obj.Category_Id;
            tbl.product_id = obj.Product_Id;
            tbl.product_details_id = obj.Product_Details_Id;
             tbl.quantity = obj.Quantity;
             tbl.request_by = obj.Request_By;


            db.mag_faram_details.Add(tbl);
            return db.SaveChanges();
        }

        public int Save()
        {
            return db.SaveChanges();
        }

        //public List<MagFaramDetailsModel> GetMagFaramByMagFaramId(int id)
        //{
        //    List<mag_faram_details> kharidmagFaramDetails = db.mag_faram_details.Where(x => x.mag_faram_id == id).ToList();
        //    List<MagFaramDetailsModel> ModelRecord = new List<MagFaramDetailsModel>();
        //    foreach (var item in kharidmagFaramDetails)
        //    {
        //        ModelRecord.Add(new MagFaramDetailsModel
        //        {
        //            Id = item.id,
        //            Category_Id =Convert.ToInt32(item.category_id),
        //            Request_By = Convert.ToInt32(item.request_by),
        //            Product_Id = Convert.ToInt32(item.product_id),
        //            Product_Details_Id = Convert.ToInt32(item.product_details_id),
        //            Quantity = Convert.ToInt32(item.quantity),

        //        });
        //    }

        //    return ModelRecord;
        //}

        public List<MagFaramDetailsModel> GetMagFaramByMagFaramId(int id)
        {
            List<MagFaramDetailsModel> ModelRecord = new List<MagFaramDetailsModel>();
            SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-GAFSDI8\SQLEXPRESS; Integrated Security=True; Database=llims");
            string sql = @"select md.request_by,c.category_name,p.product_name,pd.specification_name,md.quantity,kd.first_name_nep
                            from mag_faram_details md

                            INNER JOIN mag_faram m
                            ON md.mag_faram_id = m.id

                            INNER JOIN category c
                            ON md.category_id = c.id

                            INNER JOIN product p 
                            ON md.product_id = p.id

                            INNER JOIN product_details pd
                            ON md.product_details_id = pd.id
                            
                            INNER JOIN karmachari_details kd
                            ON md.request_by = kd.id 

                            where md.mag_faram_id =" + id.ToString();
            SqlCommand cmd = new SqlCommand(sql,con);
            SqlDataReader dr = null;
            con.Open();
            dr = cmd.ExecuteReader();
            while(dr.Read())
            {
                MagFaramDetailsModel cd = new MagFaramDetailsModel();

                cd.KarmaChari = dr["first_name_nep"].ToString();
                cd.Category_Name = dr["category_name"].ToString();
                cd.Product_Name = dr["product_name"].ToString();
                cd.Product_Specification = dr["specification_name"].ToString();
                cd.Quantity = Convert.ToInt32(dr["quantity"]);
                ModelRecord.Add(cd);

            }
            dr.Close();
            con.Close();
            return ModelRecord;

        }

        public List<MagFaramDetailsModel> GetMagFaramDetailsByMagFaramId(int id)
        {
            List<MagFaramDetailsModel> lstMagFaram = new List<MagFaramDetailsModel>();
            SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-GAFSDI8\SQLEXPRESS; Integrated Security=True; Database=llims");
            string sql = @"select md.mag_faram_id, md.request_by,md.product_id,md.quantity,m.created_date,m.mag_faram_number
                            from mag_faram_details md
                            INNER JOIN mag_faram m
                            on md.mag_faram_id = m.id
                            
                            
                              where kd.kharid_aadesh_id =" + id.ToString();
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataReader dr = null;
            con.Open();
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                MagFaramDetailsModel cd = new MagFaramDetailsModel();
                cd.Product_Id = Convert.ToInt32(dr["product_id"]);
                cd.Request_By = Convert.ToInt32(dr["request_by"]);
                cd.Quantity = Convert.ToInt32(dr["quantity"]);
                cd.Created_Date = dr["created_date"].ToString();
                cd.Mag_Faram_Number = Convert.ToInt32(dr["mag_faram_number"]);

            }
            dr.Close();
            con.Close();
            return lstMagFaram;

        }
    }
}
