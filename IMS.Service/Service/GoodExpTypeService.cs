﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IMS.Service.Inteface;
using IMS.Service.Models;
using IMS.Repository.Entities;
using System.Web.Mvc;

namespace IMS.Service.Service
{
    public class GoodExpTypeService : IGoods_exp_type
    {
        private llimsEntities db = null;

        public GoodExpTypeService()
        {
            this.db = new llimsEntities();
        }

        public List<GoodExpTypeModel> GetAll()
        {
            List<saman_barga> Record = db.saman_barga.ToList();
            List<GoodExpTypeModel> ModelRecord = new List<GoodExpTypeModel>();
            foreach (var item in Record)
            {// you can map entity to model here
                ModelRecord.Add(new GoodExpTypeModel
                {
                    Id = item.id,
                    Name = item.saman_barga_name,
                   // Status = item.status

                });
            }
            return ModelRecord;
        }

        public List<SelectListItem> GetSamanBargaList()
        {
            List<saman_barga> Record = db.saman_barga.ToList();
            List<SelectListItem> modelRecord = new List<SelectListItem>();
            foreach (var item in Record)
            {
                SelectListItem single = new SelectListItem
                {
                    Value = item.id.ToString(),
                    Text = item.saman_barga_name,
                };
                modelRecord.Add(single);

            };
            return modelRecord;
        }

        public int Insert(GoodExpTypeModel obj)
        {
            saman_barga tbl = new saman_barga();
            tbl.saman_barga_name = obj.Name;
            db.saman_barga.Add(tbl);
            return db.SaveChanges();
        }

        public int Save()
        {
            return db.SaveChanges();
        }

        public int SelectByID(int id)
        {
            throw new NotImplementedException();
        }

        public int Update(GoodExpTypeModel obj)
        {
            saman_barga tbl = new saman_barga();
            tbl = db.saman_barga.Where(u => u.id == obj.Id).FirstOrDefault();
            tbl.saman_barga_name = obj.Name;
            return db.SaveChanges();
        }

        public int Delete(int id)
        {
            saman_barga samanBarga = db.saman_barga.Where(u => u.id == id).FirstOrDefault();
            db.saman_barga.Remove(samanBarga);
            return db.SaveChanges();
        }

       

    }
}
