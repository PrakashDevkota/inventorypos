﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using IMS.Repository.Entities;
using IMS.Service.Inteface;
using IMS.Service.Models;

namespace IMS.Service.Service
{
    public class CategoryService : ICategoryService
    {
        
        private llimsEntities db = null;
        public CategoryService()
        {
            this.db = new llimsEntities();
        }
        
        public List<CategoryModel> GetAll()
        {
            List<category> Record = db.categories.ToList(); 
            List<CategoryModel> ModelRecord = new List<CategoryModel>();
           
            
            foreach (var item in Record)
            {// you can map entity to model here
                ModelRecord.Add(new CategoryModel
                {
                    Id = item.id,
                    Category_name = item.category_name,
                   Parent_id = Convert.ToInt32(item.parent_id),
                   
                   
                });
            }
            return ModelRecord;
        }


        public List<SelectListItem> GetCategorySelectListItem()
        {
            List<category> Record = db.categories.ToList();
            List<SelectListItem> modelRecord = new List<SelectListItem>();
            foreach (var item in Record)
            {
                SelectListItem single = new SelectListItem
                {
                    Value = item.id.ToString(),
                    Text = item.category_name,
                };
                modelRecord.Add(single);

            };
            return modelRecord;
        }
        public List<CategoryModel> GetAllCategory()
        {
            List<category> Record = db.categories.ToList();

            List<CategoryModel> modelRecord = new List<CategoryModel>();

            foreach (var item in Record)
            {// you can map entity to model here
                modelRecord.Add(new CategoryModel
                {
                    Id = item.id,
                    Category_name = item.category_name,
                });
            }
            return modelRecord;
        }

        public int Insert(CategoryModel obj)
        {
            category tbl = new category();
            tbl.category_name = obj.Category_name;
            tbl.parent_id = obj.Parent_id;
           

            db.categories.Add(tbl);
            return db.SaveChanges();
        }

        public int Save()
        {
            return db.SaveChanges();
        }

        public int SelectByID(int id)
        {
            return 1;
        }

        public int Update(CategoryModel obj)
        {
            category tbl = new category();
            tbl = db.categories.Where(u => u.id == obj.Id).FirstOrDefault();
            // tbl.id = obj.Id;
            tbl.category_name = obj.Category_name;
            tbl.parent_id = obj.Parent_id;
            
            return db.SaveChanges();
        }

        public int Delete(int id)
        {
            category tbl = db.categories.Where(u => u.id == id).FirstOrDefault();
            db.categories.Remove(tbl);
            return db.SaveChanges();
        }
    }
}
