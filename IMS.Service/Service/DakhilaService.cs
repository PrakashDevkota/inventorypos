﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IMS.Service.Inteface;
using IMS.Service.Models;
using IMS.Repository.Entities;

namespace IMS.Service.Service
{
    public class DakhilaService : IDakhilaService
    {

        private llimsEntities db = null;
        public DakhilaService()
        {
            this.db = new llimsEntities();
        }
        public List<DakhilaModel> GetAll()
        {
            throw new NotImplementedException();
        }

        public List<DakhilaModel> GetAllDakhila()
        {
            throw new NotImplementedException();
        }

        public int Insert(DakhilaModel obj)
        {
            dakhila tbl = new dakhila();
            tbl.kharid_aadesh_id = obj.Kharid_adesh_id;
            tbl.status = 0;
            tbl.created_by = obj.Created_By;
            tbl.office_id = Convert.ToInt32(obj.Office_id);
            tbl.created_date = obj.Created_date;
            tbl.fiscal_year = "2075/76";
            db.dakhilas.Add(tbl);
            db.SaveChanges();
            int Id = tbl.id;
            return Id;
        }

        public int Save()
        {
            throw new NotImplementedException();
        }

        public int SelectByID(int id)
        {
            throw new NotImplementedException();
        }

        public int Update(DakhilaModel obj)
        {
            throw new NotImplementedException();
        }

        public int Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}
