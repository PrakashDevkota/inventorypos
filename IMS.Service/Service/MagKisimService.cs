﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IMS.Repository.Entities;
using IMS.Service.Inteface;
using IMS.Service.Models;

namespace IMS.Service.Service
{
    public class MagKisimService : IMagKisimService
    {

        private llimsEntities db;
        public MagKisimService()
        {
            this.db = new llimsEntities();
        }
        public List<MaagKisimModel> GetAll()
        {
            List<mag_kisim> Record = db.mag_kisim.ToList();
            List<MaagKisimModel> ModelRecord = new List<MaagKisimModel>();
            foreach (var item in Record)
            {// you can map entity to model here
                ModelRecord.Add(new MaagKisimModel
                {
                    Id = item.id,
                    Name = item.name,
                    // Status = item.status

                });
            }
            return ModelRecord;
        }

        public int Insert(MaagKisimModel obj)
        {
            mag_kisim tbl = new mag_kisim();
            tbl.name = obj.Name;
            db.mag_kisim.Add(tbl);
            return db.SaveChanges();
        }

        public int Save()
        {
            return db.SaveChanges();
        }

        public int SelectByID(int id)
        {
            throw new NotImplementedException();
        }

        public int Update(MaagKisimModel obj)
        {
            mag_kisim tbl = new mag_kisim();
            tbl = db.mag_kisim.Where(u => u.id == obj.Id).FirstOrDefault();
            tbl.name = obj.Name;
            return db.SaveChanges();
        }

        public int Delete(int id)
        {
            mag_kisim samanPrakriti = db.mag_kisim.Where(u => u.id == id).FirstOrDefault();
            db.mag_kisim.Remove(samanPrakriti);
            return db.SaveChanges();
        }
    }
}
