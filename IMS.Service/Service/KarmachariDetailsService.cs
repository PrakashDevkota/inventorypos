﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using IMS.Service.Inteface;
using IMS.Service.Models;
using IMS.Repository.Entities;

namespace IMS.Service.Service
{
    public class KarmachariDetailsService : IKarmachariDetailsService
    {
        private llimsEntities db;
         public KarmachariDetailsService()
        {
            this.db = new llimsEntities();
        }

        public List<SelectListItem> GetKarmachariByOfficeId(string id)
        {
            List<KarmachariDetailsModel> lstKarmachari = new List<KarmachariDetailsModel>();
            SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-GAFSDI8\SQLEXPRESS; Integrated Security=True; Database=llims");
            string sql = @"select kd.first_name_nep,kd.id
                         from office o 
                        INNER JOIN karmachari k
                        On o.id= k.office_id
                        INNER JOIN karmachari_details kd
                        On k.id = kd.karmachari_id
                        where o.id =" + id.ToString();
            SqlCommand cmd = new SqlCommand(sql, con);
            //cmd.Parameters.AddWithValue("@users.id", id);
            SqlDataReader dr = null;
            con.Open();
            dr = cmd.ExecuteReader();
          List<SelectListItem> cd = new List<SelectListItem>();

            while (dr.Read())
            {

                SelectListItem single = new SelectListItem
                {
                    Value = dr["id"].ToString(),
                    Text = (string)dr["first_name_nep"],
            };
                cd.Add(single);
            }
            dr.Close();
            con.Close();
            return cd;
        }


        public List<SelectListItem> getKarmachariDetailsByOfficeId(int id)
        {
            List<karmachari_details> Record = db.karmachari_details.Where(x => x.office == id).ToList();
            List<SelectListItem> modelRecord = new List<SelectListItem>();
            foreach (var item in Record)
            {
                SelectListItem single = new SelectListItem
                {
                    Value = item.id.ToString(),
                    Text = item.first_name_nep.ToString(),
                };
                modelRecord.Add(single);

            };
            return modelRecord;
        }
        public int insert(KarmachariDetailsModel obj, int id)
        {
            karmachari_details tbl = new karmachari_details();
            tbl.karmachari_id = id;
            tbl.first_name_nep = obj.First_Name;
            tbl.last_name_nep = obj.Last_Name;
            tbl.sewa = obj.Sewa;
            tbl.smuha = obj.Samuha;
            tbl.office = obj.Office_Id;
            db.karmachari_details.Add(tbl);
            return db.SaveChanges();
        }

      

        public int update(KarmachariDetailsModel obj)
        {
            throw new NotImplementedException();
        }

        public int delete(int id)
        {
            throw new NotImplementedException();
        }

        public int InsertFormAuthority(FormAuthorityModel obj)
        {
            form_authority tbl = new form_authority();
            tbl.form_ready_by = obj.Ready_By;
            tbl.form_submit_by = obj.Submit_By;
            tbl.form_approved_by = obj.Approved_By;
            tbl.office_id = obj.Office_Id;
            db.form_authority.Add(tbl);
            return db.SaveChanges();
        }
    }
}

