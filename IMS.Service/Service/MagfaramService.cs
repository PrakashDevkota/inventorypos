﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IMS.Service.Inteface;
using IMS.Service.Service;
using IMS.Repository.Entities;
using IMS.Service.Models;
using System.Web.Mvc;
using System.Data.SqlClient;

namespace IMS.Service.Service
{
   
   public class MagfaramService : IMagfaramService
    {
        private llimsEntities db;
        public MagfaramService()
        {
            this.db = new llimsEntities();
        }

       

        public List<SelectListItem> GetAll()
        {
            List<mag_faram> Record = db.mag_faram.ToList();
            List<SelectListItem> modelRecord = new List<SelectListItem>();
            foreach (var item in Record)
            {
                SelectListItem single = new SelectListItem
                {
                    Value = item.id.ToString(),
                    Text = item.mag_faram_number.ToString(),
                };
                modelRecord.Add(single);

            };
            return modelRecord;
        }

        public List<SelectListItem> GetMagFaramIdByOfficeId(int office_id)
        {
            List<mag_faram> Record = db.mag_faram.Where(m => m.office_id == office_id).ToList();
            List<SelectListItem> modelRecord = new List<SelectListItem>();
            foreach (var item in Record)
            {
                SelectListItem single = new SelectListItem
                {
                    Value = item.id.ToString(),
                    Text = item.mag_faram_number.ToString(),
                };
                modelRecord.Add(single);

            };
            return modelRecord;
        }
       
       

        public int Insert(MagfaramModel obj)
        {
            mag_faram tbl = new mag_faram();
            tbl.mag_faram_number = obj.Mag_faram_number;
            tbl.office_id = obj.Office_id;
            tbl.created_by = obj.Created_by;
            tbl.created_date = obj.Created_date;
            tbl.fiscal_year = "2075 /76";

            db.mag_faram.Add(tbl);
             db.SaveChanges();
            int id = tbl.id;
            return id;
        }

        public int Delete(int id)
        {
            mag_faram tbl = db.mag_faram.Where(u => u.id == id).FirstOrDefault();
            db.mag_faram.Remove(tbl);
            return db.SaveChanges();
        }

        public int Save()
        {
            throw new NotImplementedException();
        }

        public int Update(MagfaramModel obj)
        {
            throw new NotImplementedException();
        }

        public int SelectByID(int id)
        {
            throw new NotImplementedException();
        }
        public List<MagfaramModel> GetAllMag()
        {
            throw new NotImplementedException();
        }

        public int getMaxMagFaramNumberByOfficeId(int office_id)
        {
            var data = db.mag_faram
                 .Where(c => c.office_id == office_id)
                 .Select(c => c.mag_faram_number)
                 .DefaultIfEmpty(0)
                 .Max();
            if (data == null)
            {
                data = Convert.ToInt32(data) + 1;
            }
            else
            {
                data = Convert.ToInt32(data) + 1;
            }
            return Convert.ToInt32(data);
        }
    }
    }

