﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IMS.Service.Inteface;
using IMS.Repository.Entities;
using IMS.Service.Models;

namespace IMS.Service.Service
{
    public class DakhilaDetailsService : IDakhilaDetailsService
    {
        private llimsEntities db = null;
        public DakhilaDetailsService()
        {
            this.db = new llimsEntities();
        }


        public int Insert(DakhilaDetailsModel obj)
        {
            dakhilaDetail tbl = new dakhilaDetail();
            tbl.dakhila_id = obj.dakhila_id;
            tbl.kharid_adesh_id = obj.Kharid_adesh_id;
            tbl.category_id = obj.Category_Id;
            tbl.product_id = obj.Product_id;
            tbl.product_details_id = obj.Product_Details_Id;
            tbl.kharid_type_id = obj.Kharid_type_id;
            tbl.quantity = obj.Quantity;
            tbl.seller_id = obj.Seller_id;

            db.dakhilaDetails.Add(tbl);
            return db.SaveChanges();
        }

        public int Save()
        {
            throw new NotImplementedException();
        }

        public int SelectByID(int id)
        {
            throw new NotImplementedException();
        }
        public List<DakhilaDetailsModel> GetAll()
        {
            throw new NotImplementedException();
        }

        public List<DakhilaDetailsModel> GetAllDakhilaDetails()
        {
            throw new NotImplementedException();
        }


        public int Update(DakhilaDetailsModel obj)
        {
            throw new NotImplementedException();
        }

        public int Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}
