﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IMS.Repository.Entities;
using IMS.Service.Inteface;
using IMS.Service.Models;

namespace IMS.Service.Service
{
    public class StoreService : IStoreService
    {
        private llimsEntities db;
        public StoreService()
        {
            this.db = new llimsEntities();
        }
        public int Insert(StoreModel obj)
        {
            store tbl = new store();
            tbl.product_id = obj.Product_Id;
            tbl.quantity = obj.Quantity;
            tbl.office_id = obj.Office_Id;
            tbl.created_by = obj.Created_By;
            tbl.created_date = obj.Created_Date;
            db.stores.Add(tbl);
            return db.SaveChanges();
        }

        public int UpdateProductByOfficeIdProductId(int product_id, int office_id, int quantity)
        {
            store tbl = new store();
            tbl = db.stores.Where(u => u.product_id == product_id && u.office_id == office_id).FirstOrDefault();
            // tbl.id = obj.Id;
            if (tbl.quantity < quantity)
            {
                return 0;
            }
            tbl.quantity = tbl.quantity - quantity;
            return db.SaveChanges();
        }
    }
}
