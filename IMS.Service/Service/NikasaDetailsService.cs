﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IMS.Repository;
using IMS.Service.Inteface;
using IMS.Service.Models;
using IMS.Repository.Entities;


namespace IMS.Service.Service
{
    public class NikasaDetailsService : INikasaDetailsService
    {
        private llimsEntities db;
        public NikasaDetailsService()
        {
            this.db = new llimsEntities();
        }

        public int Insert(NikasaDetailsModel obj)
        {
            nikasa_details tbl = new nikasa_details();
            tbl.nikasa_id = obj.Nikasa_Id;
            tbl.product_id = obj.Product_id;
            tbl.quantity = obj.Quantity;
           

            db.nikasa_details.Add(tbl);
            return db.SaveChanges();

        }
    }
}
