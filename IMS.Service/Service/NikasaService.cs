﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IMS.Service.Inteface;
using IMS.Repository.Entities;
using IMS.Service.Models;

namespace IMS.Service.Service
{
    public class NikasaService : INikasaService
    {
       private llimsEntities db;
        public NikasaService()
        {
            this.db = new llimsEntities();
        }
        public int Insert(NikasaModel obj)
        {
            nikasa tbl = new nikasa();
            tbl.mag_faram_id = obj.Mag_Faram_Id;
            tbl.office_id = obj.Office_Id;
            tbl.created_by = obj.Created_By;
            tbl.created_date = obj.Created_Date;
            tbl.status = 0;

            db.nikasas.Add(tbl);
            db.SaveChanges();
            int Id = tbl.id;
            return Id;
           
           
        }
    }
}
