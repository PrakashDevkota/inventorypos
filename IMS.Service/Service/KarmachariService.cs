﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IMS.Service.Inteface;
using IMS.Service.Models;
using IMS.Repository.Entities;

namespace IMS.Service.Service
{
    public class KarmachariService : IKarmachariService
    {

        private llimsEntities db = null;

        public KarmachariService()
        {
            this.db = new llimsEntities();
        }
        public List<KarmachariModel> GetAll()
        {
            List<karmachari> Record = db.karmacharis.ToList();
            List<KarmachariModel> ModelRecord = new List<KarmachariModel>();


            foreach (var item in Record)
            {// you can map entity to model here
                ModelRecord.Add(new KarmachariModel
                {
                    Id = item.id,
                    Office_Id = Convert.ToInt32(item.office_id),
                   // Name = item.name,
                   


                });
            }
            return ModelRecord;
        }

      
        public int Insert(KarmachariModel obj)
        {
            karmachari tbl = new karmachari();

            tbl.office_id = obj.Office_Id;
            tbl.created_by = obj.Created_by;
            tbl.status = 1;
            db.karmacharis.Add(tbl);
            db.SaveChanges();
            int id = tbl.id;
           
            return id;
        }

        public int Save()
        {
            throw new NotImplementedException();
        }

        public int SelectByID(int id)
        {
            throw new NotImplementedException();
        }

        public int Update(KarmachariModel obj)
        {
            throw new NotImplementedException();
        }

        public int Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}
