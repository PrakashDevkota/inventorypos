﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IMS.Service.Inteface;
using IMS.Service.Models;
using IMS.Repository.Entities;
using System.Web.Mvc;

namespace IMS.Service.Service
{
    public class ProductService : IProductService
    {

        private llimsEntities db;
        public ProductService()
        {
            this.db = new llimsEntities();
        }
        public List<ProductModel> GetAll()
        {
            //List<product> EntityRecord = db.products.ToList();
            List<ProductModel> ModelRecord = new List<ProductModel>();
            var entityrecord = (from c in db.categories
                                from sp in db.saman_barga
                                from s in db.products
                                
                                where c.id == s.category_id
                                where sp.id == s.saman_barga_id
                                select new
                                {
                                    category_id = c.id,
                                    id = s.id,
                                  category_name = c.category_name,
                                  product_name = s.product_name,
                                  slug = s.slug,
                                  saman_barga_id = s.saman_barga_id,
                                  saman_barga_nam = sp.saman_barga_name,
                                  
                                   

                                }).ToList();
            foreach (var item in entityrecord)
            {
                ModelRecord.Add(new ProductModel
                {
                   Id = item.id,
                    Product_name = item.product_name,
                    Category_name = item.category_name,
                   // Category_id = Convert.ToInt32(item.category_id),
                    Slug = item.slug,
                    Saman_Barga_Name = item.saman_barga_nam,
                });
            }
            return ModelRecord;
        }

        public List<ProductModel> GetAllProduct()
        {
            List<product> EntityRecord = db.products.ToList();
            List<ProductModel> ModelRecord = new List<ProductModel>();
            foreach (var item in EntityRecord)
            {
                ModelRecord.Add(new ProductModel
                {
                    Id = item.id,
                    Product_name = item.product_name,
                  
                });
            }
            return ModelRecord;
        }

        public List<SelectListItem> GetProductSelectListItem()
        {
            List<ProductModel> Record = this.GetAllProduct();
            List<SelectListItem> modelRecord = new List<SelectListItem>();
            foreach (var item in Record)
            {
                SelectListItem single = new SelectListItem
                {
                    Value = item.Id.ToString(),
                    Text = item.Product_name,
                };
                modelRecord.Add(single);

            };
            return modelRecord;
        }

        

        public int Insert(ProductModel obj)
        {
            product tbl = new product();
            tbl.category_id = obj.Category_Id;
            tbl.product_name = obj.Product_name;
            tbl.unit_id = obj.Unit_Id;  
            tbl.saman_barga_id = obj.Saman_barga_id;
            tbl.office_id = obj.Office_Id;
            //tbl.slug = obj.Slug;

            db.products.Add(tbl);
            return db.SaveChanges();
        }

        public int Save()
        {
            return db.SaveChanges();
        }

        public int SelectByID(int id)
        {
            throw new NotImplementedException();
        }

        public int Update(ProductModel obj)
        {
            product tbl = new product();
            tbl = db.products.Where(p => p.id.Equals(obj.Id)).FirstOrDefault();
            tbl.product_name = obj.Product_name;
            tbl.category_id = obj.Category_Id;
            tbl.slug = obj.Slug;

            return db.SaveChanges();
        }
        public int Delete(int id)
        {
            product pro = db.products.Where(p => p.id == id).FirstOrDefault();
            db.products.Remove(pro);
           return db.SaveChanges();
        }

        public List<ProductModel> SelectCategoryByproductId()
        {

            List<ProductModel> ModelRecord = new List<ProductModel>();
            var categoryList = (from c in db.categories
                               from s in db.products
                               where c.id == s.category_id
                               select new
                               {
                                   //category_id = c.id,
                                   category_name = c.category_name,
                                   //product_name = s.product_name,
                                  // slug = s.slug,


                               }).ToList();
            foreach (var item in categoryList)
            {
                ModelRecord.Add(new ProductModel
                {
                   
                    Category_name = item.category_name,
                });
            }
            return ModelRecord;
        }
       
        public List<ProductModel> getProductByOfficeIdAndCategoryId(int category_id, int office_id)
        {
           
            List<ProductModel> productList = new List<ProductModel>();
            List<product> tbl = db.products.Where(c => c.category_id == category_id && c.office_id == office_id).ToList();
            foreach (var item in tbl)
            {
                productList.Add(new ProductModel
                {
                    Id = item.id,
                    Product_name = item.product_name,
                });
            }

            return productList;
        }
    }
}
