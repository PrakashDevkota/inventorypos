﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IMS.Service.Inteface;
using IMS.Service.Models;
using IMS.Repository.Entities;
using System.Web.Mvc;
using System.Data.SqlClient;

namespace IMS.Service.Service
{
    public class KharidAdesService : IKharidAdesService
    {

        private llimsEntities db;
        public KharidAdesService()
        {
            this.db = new llimsEntities();
        }

        public List<KharidAdeshModel> GetAllKharidAdes()
        {
            List<kharid_aadesh> Record = db.kharid_aadesh.ToList(); 
            List<KharidAdeshModel> ModelRecord = new List<KharidAdeshModel>();
           
            foreach (var item in Record)
            {// you can map entity to model here
                ModelRecord.Add(new KharidAdeshModel
                {
                    Id = item.id,
                    KharidAdesNumber = Convert.ToInt32(item.kharid_ades_number),
                    product_id = Convert.ToInt32(item.product_id),
                    Status = Convert.ToInt32(item.status),
                    //date = Convert.ToDateTime(item.date)

                });
            }
            return ModelRecord;
        }

        public List<KharidAdeshModel> GetKharidListItem()
        {
            List<kharid_aadesh> Record = db.kharid_aadesh.ToList();
            List<KharidAdeshModel> ModelRecord = new List<KharidAdeshModel>();

            foreach (var item in Record)
            {// you can map entity to model here
                KharidAdeshModel single = new KharidAdeshModel
                {
                    Id = item.id,
                    KharidAdesNumber = Convert.ToInt32(item.kharid_ades_number),
                    product_id = Convert.ToInt32(item.product_id),
                    Status = Convert.ToInt32(item.status),
                    //date = Convert.ToDateTime(item.date)

                };
                ModelRecord.Add(single);
            }
            return ModelRecord;
        }

        public List<SelectListItem> GetKharidListItems()
        {
            List<kharid_aadesh> Record = db.kharid_aadesh.ToList();
            List<SelectListItem> modelRecord = new List<SelectListItem>();
            foreach (var item in Record)
            {
                SelectListItem single = new SelectListItem
                {
                    Value = item.id.ToString(),
                    Text = item.kharid_ades_number.ToString(),
                };
                modelRecord.Add(single);

            };
            return modelRecord;
        }

        public List<SelectListItem> GetKharidListItemsByOfficId(int office_id)
        {
            List<kharid_aadesh> Record = db.kharid_aadesh.Where(o => o.office_id == office_id).ToList();
            List<SelectListItem> modelRecord = new List<SelectListItem>();
            foreach (var item in Record)
            {
                SelectListItem single = new SelectListItem
                {
                    Value = item.id.ToString(),
                    Text = item.kharid_ades_number.ToString(),
                };
                modelRecord.Add(single);

            };
            return modelRecord;
        }



        public List<KharidAdeshModel> GetAll()
        {
             //List<kharid_aadesh> Record = db.kharid_aadesh.ToList(); 
            List<KharidAdeshModel> ModelRecord = new List<KharidAdeshModel>();
            var Record = (from p in db.products
                          from k in db.kharid_aadesh
                          where p.id == k.product_id

                          select new
                          {
                              id = k.id,
                              kharid_ades_number = k.kharid_ades_number,
                              product_name = p.product_name,

                          }).ToList();

            foreach (var item in Record)
            {// you can map entity to model here
                ModelRecord.Add(new KharidAdeshModel
                {
                    Id = item.id,
                    Product_name = item.product_name,
                    KharidAdesNumber = Convert.ToInt32(item.kharid_ades_number),
                    //product_id = Convert.ToInt32(item.product_id),
                    //date = Convert.ToDateTime(item.date)
                   
                });
            }
            return ModelRecord;
        }

       
        public int Insert(KharidAdeshModel obj)
        {
            kharid_aadesh tbl = new kharid_aadesh();
            tbl.kharid_ades_number = obj.KharidAdesNumber;
            tbl.product_id = obj.product_id;
            tbl.office_id = Convert.ToInt32(obj.Office_id);
            tbl.created_by = Convert.ToInt32(obj.Created_by);
            tbl.created_date = obj.CreatedDate;
            tbl.status = 1;
            tbl.fiscal_year = "2075/76";
            db.kharid_aadesh.Add(tbl);
            
            db.SaveChanges();
            int Id = tbl.id;
            return Id;
            
        }

        public int Save()
        {
            return db.SaveChanges();
        }

        public int SelectByID(int id)
        {
            throw new NotImplementedException();
        }

        public int Update(KharidAdeshModel obj)
        {
            kharid_aadesh tbl = new kharid_aadesh();
            tbl = db.kharid_aadesh.Where(u => u.id == obj.Id).FirstOrDefault();
            // tbl.id = obj.Id;
            tbl.kharid_ades_number = obj.KharidAdesNumber;
            tbl.product_id = obj.product_id;
            //tbl.date = obj.date;
            return db.SaveChanges();
        }

        public int Delete(int id)
        {
            kharid_aadesh tbl = db.kharid_aadesh.Where(u => u.id == id).FirstOrDefault();
            db.kharid_aadesh.Remove(tbl);
            return db.SaveChanges();
        }

        public int getKharidAdeshNumberByOfficeId(int office_id)
        {
          var data =  db.kharid_aadesh
                .Where(c => c.office_id == office_id)
                .Select(c => c.kharid_ades_number)
                .DefaultIfEmpty(0)
                .Max();
            if (data == null)
            {
                data = Convert.ToInt32(data) + 1;
            }
            else
            {
                data = Convert.ToInt32(data) + 1;
            }
            return Convert.ToInt32(data);
        }

        public List<KharidAdeshModel> getAllKharidAdeshByOfficeId(int office_id)
        {
            List<kharid_aadesh> tbl = db.kharid_aadesh.Where(u => u.office_id == office_id).ToList();

            List<KharidAdeshModel> model = new List<KharidAdeshModel>();

            foreach (var item in tbl)
            {
                model.Add(new KharidAdeshModel
                {
                    Id = item.id,
                KharidAdesNumber = Convert.ToInt32(item.kharid_ades_number),
                CreatedDate = item.created_date,

                });
            }
            return model;
            
        }
    }

    
}
