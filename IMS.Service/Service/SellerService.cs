﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using IMS.Repository.Entities;
using IMS.Service.Inteface;
using IMS.Service.Models;


namespace IMS.Service.Service
{
   public class SellerService : ISellerService
    {
        private llimsEntities db;
        public SellerService()
        {
            this.db = new llimsEntities();
        }

       

        public List<SellerModel> GetSellerByOfficeId1(int id)
        {
            List<seller> Record = db.sellers.Where(x => x.id == id).ToList();
            List<SellerModel> ModelRecord = new List<SellerModel>();
            foreach (var item in Record)
            {// you can map entity to model here
                ModelRecord.Add(new SellerModel
                {
                    Id = item.id,
                    Name = item.company_name,
                    Phone = item.phone,
                    Mobile = item.mobile,
                    Darta_number = item.darta_number,
                    Owner_name = item.owner_name,

                });
            }
            return ModelRecord;
        }

        public List<SelectListItem> GetSellerByOfficeId(int id)
        {
            List<seller> Record = db.sellers.Where(x => x.id == id).ToList();
            List<SelectListItem> modelRecord = new List<SelectListItem>();
            foreach (var item in Record)
            {
                SelectListItem single = new SelectListItem
                {
                    Value = item.id.ToString(),
                    Text = item.company_name.ToString(),
                };
                modelRecord.Add(single);

            };
            return modelRecord;

        }


        public int Insert(SellerModel obj)
        {
            seller tbl = new seller();
            tbl.office_id = obj.Office_Id;
            tbl.company_name = obj.Name;
            tbl.phone = obj.Phone;
            tbl.mobile = obj.Mobile;
            tbl.darta_number = obj.Darta_number;
            tbl.owner_name = obj.Owner_name;
            tbl.address = obj.Address;
            tbl.status = 1;
            tbl.created_by = obj.Created_By;
            tbl.created_date = obj.Created_Date;
            db.sellers.Add(tbl);
            return db.SaveChanges();
        }

        public int Save()
        {
            return db.SaveChanges();
        }

        public int SelectByID(int id)
        {
            throw new NotImplementedException();
        }

        public int Update(SellerModel obj)
        {
            seller tbl = new seller();
            tbl = db.sellers.Where(u => u.id == obj.Id).FirstOrDefault();
            // tbl.id = obj.Id;
            tbl.company_name = obj.Name;

            return db.SaveChanges();
        }

        public int Delete(int id)
        {
            seller tbl = db.sellers.Where(u => u.id == id).FirstOrDefault();
            db.sellers.Remove(tbl);
            return db.SaveChanges();
        }

        public List<SelectListItem> SellerLIst()
        {
            List<seller> entityRecord = db.sellers.ToList();
            List<SelectListItem> modelRecord = new List<SelectListItem>();
            foreach (var item in entityRecord)
            {
                SelectListItem single = new SelectListItem
                {
                    Value = item.id.ToString(),
                    Text = item.company_name.ToString(),
                };
                modelRecord.Add(single);

            };
            return modelRecord;
            
        }
    }
}
