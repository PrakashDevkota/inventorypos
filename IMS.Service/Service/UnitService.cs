﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using IMS.Repository.Entities;
using IMS.Service.Inteface;
using IMS.Service.Models;

namespace IMS.Service.Service
{
    public class UnitService : IUnitService
    {
        private DbSet<unit> table = null;

        private llimsEntities db = null;
        public UnitService()
        {
            this.db = new llimsEntities();
            table = db.Set<unit>();
        }
       
        public List<UnitModel> GetAll()
        {
            List <unit> Record = db.units.ToList();
            List<UnitModel> ModelRecord = new List<UnitModel>();
            foreach (var item in Record)
            {// you can map entity to model here

                AutoMapper.Mapper.Map(Record, ModelRecord);
                //ModelRecord.Add(new UnitModel
                //{
                //    Id = item.id,
                //    Name = item.unit_name,
                //    Status = Convert.ToInt16(item.status)

                //});
            }
            return ModelRecord;
        }

        public int Insert(UnitModel unitModel)
        {
            unit tbl = new unit();
            tbl.unit_name = unitModel.Name;
            tbl.status = unitModel.Status;

            db.units.Add(tbl);
             return db.SaveChanges();
        }

       

        public int Update(UnitModel obj)
        {
            unit tbl = new unit();
            tbl = db.units.Where(u => u.id == obj.Id).FirstOrDefault();
           // tbl.id = obj.Id;
            tbl.unit_name = obj.Name;
            tbl.status = obj.Status;
            return db.SaveChanges();
        }
        public int Save()
        {
            return db.SaveChanges();
        }


        public int Delete(int id)
        {
            unit tbl = db.units.Where(u => u.id == id).FirstOrDefault();
            db.units.Remove(tbl);
            return db.SaveChanges();
        }

        public int SelectByID(int id)
        {
            return 1;
            
        }

        public List<SelectListItem> UnitSelectItem()
        {
            List<unit> Record = db.units.ToList();
            List<SelectListItem> modelRecord = new List<SelectListItem>();
            foreach (var item in Record)
            {
                SelectListItem single = new SelectListItem
                {
                    Value = item.id.ToString(),
                    Text = item.unit_name
                };
                modelRecord.Add(single);

            };
            return modelRecord;
        }
    }
}
