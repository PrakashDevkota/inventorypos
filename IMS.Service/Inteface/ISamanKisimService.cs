﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using IMS.Service.Models;

namespace IMS.Service.Inteface
{
   public interface ISamanKisimService
    {
        List<SamanKisimModel> GetAll();
        List<SamanKisimModel> GetAllSamanKisim();
        List<SelectListItem> GetSamanKisimSelectListItem();
        int SelectByID(int id);
        int Insert(SamanKisimModel obj);
        int Update(SamanKisimModel obj);
        int Delete(int id);
        int Save();
    }
}
