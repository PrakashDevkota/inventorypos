﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IMS.Service.Models;
using IMS.Repository.Entities;
using System.Web.Mvc;

namespace IMS.Service.Inteface
{
    public interface IUnitService
    {
        List<UnitModel> GetAll();

        List<SelectListItem> UnitSelectItem();
        int SelectByID(int id);
        int Insert(UnitModel obj);
        int Update(UnitModel obj);
        int Delete(int id);
        int Save();
    }
}
