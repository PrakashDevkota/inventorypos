﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using IMS.Service.Models;

namespace IMS.Service.Inteface
{
   public interface IKharidAdesService
    {
        List<KharidAdeshModel> GetAll();
        List<KharidAdeshModel> GetAllKharidAdes();
        List<KharidAdeshModel> GetKharidListItem();

        List<KharidAdeshModel> getAllKharidAdeshByOfficeId(int id);

        List<SelectListItem> GetKharidListItems();
        List<SelectListItem> GetKharidListItemsByOfficId(int id);
        int getKharidAdeshNumberByOfficeId(int id);
        int SelectByID(int id);
        int Insert(KharidAdeshModel obj);
        int Update(KharidAdeshModel obj);
        int Delete(int id);
        int Save();
    }
}
