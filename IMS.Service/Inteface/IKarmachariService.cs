﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IMS.Service.Models;

namespace IMS.Service.Inteface
{
  public  interface IKarmachariService
    {
        List<KarmachariModel> GetAll();
        
        int SelectByID(int id);
        int Insert(KarmachariModel obj);
        int Update(KarmachariModel obj);
        int Delete(int id);
        int Save();
    }
}
