﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using IMS.Service.Models;

namespace IMS.Service.Inteface
{
   public interface IKharidAdesDetailsService
    {
        List<KharidAdesDetailsModel> GetAll();
        List<KharidAdesDetailsModel> GetByKharidAdeshId(int id);
        List<SelectListItem> GetKharidAdesDetailsList();
        List<KharidAdesDetailsModel> GetKharidAdeshDetailsByKharidAdeshId(int id);
        int SelectByID(int id);
        int Insert(KharidAdesDetailsModel obj);
        int Update(KharidAdesDetailsModel obj);
        int Delete(int id);
        int Save();

        List<KharidAdesDetailsModel> getKharidAdeshDetailsByKharidAdeshIdAndOfficeId(int kharidAdesh_id, int office_id);
        List<KharidAdesDetailsModel> getKharidAdeshDetailsByOfficeId(int id);
    }
}
