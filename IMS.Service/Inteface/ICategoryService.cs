﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using IMS.Service.Models;

namespace IMS.Service.Inteface
{
   public interface ICategoryService
    {
        //this is the test

            //thisidfdf
            //hunter
            //hunter2
        List<CategoryModel> GetAll();
        List<SelectListItem> GetCategorySelectListItem();
        List<CategoryModel> GetAllCategory();
        int SelectByID(int id);
        int Insert(CategoryModel obj);
        int Update(CategoryModel obj);
        int Delete(int id);
        int Save();
    }
}
