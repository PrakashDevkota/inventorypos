﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using IMS.Service.Models;

namespace IMS.Service.Inteface
{
   public interface IKharidTypeService
    {
        List<KharidTypeModel> GetAll();
        List<SelectListItem> GetKharidPrakarSelectListItem();
        int SelectByID(int id);
        int Insert(KharidTypeModel obj);
        int Update(KharidTypeModel obj);
        int Delete(int id);
        int Save();
    }
}
