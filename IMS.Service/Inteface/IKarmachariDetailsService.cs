﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using IMS.Service.Models;

namespace IMS.Service.Inteface
{
   public interface IKarmachariDetailsService
    {
        List<SelectListItem> GetKarmachariByOfficeId(string id);

        int insert(KarmachariDetailsModel obj, int id);

        int update(KarmachariDetailsModel obj);

        int delete(int id);

        List<SelectListItem> getKarmachariDetailsByOfficeId(int id);

        int InsertFormAuthority(FormAuthorityModel obj);
    }
}
