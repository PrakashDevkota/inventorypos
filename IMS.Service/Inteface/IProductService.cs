﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using IMS.Service.Models;

namespace IMS.Service.Inteface
{
   public interface IProductService
    {
        List<ProductModel>  GetAll();
        List<SelectListItem> GetProductSelectListItem();
        List<ProductModel> GetAllProduct();
        int SelectByID(int id);
        int Insert(ProductModel obj);
        int Update(ProductModel obj);
        int Delete(int id);
        int Save();
        List<ProductModel> SelectCategoryByproductId();

        List<ProductModel> getProductByOfficeIdAndCategoryId(int id1, int id2);
    }
}
