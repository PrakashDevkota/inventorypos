﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IMS.Service.Models;

namespace IMS.Service.Inteface
{
   public interface IDakhilaDetailsService
    {
        List<DakhilaDetailsModel> GetAll();

        List<DakhilaDetailsModel> GetAllDakhilaDetails();

        int SelectByID(int id);

        int Insert(DakhilaDetailsModel obj);

        int Update(DakhilaDetailsModel obj);

        int Delete(int id);

        int Save();
    }
}
