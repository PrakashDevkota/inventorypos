﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using IMS.Service.Models;

namespace IMS.Service.Inteface
{
   public interface ISamankoPrakritiService
    {
        List<SamankoPrakritiModel> GetAll();
        
        int SelectByID(int id);
        List<SelectListItem> GetSamanPrakritiSelectListItem();
        int Insert(SamankoPrakritiModel obj);
        int Update(SamankoPrakritiModel obj);
        int Delete(int id);
        int Save();
    }
}
