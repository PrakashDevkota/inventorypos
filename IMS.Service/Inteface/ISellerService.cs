﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using IMS.Service.Models;

namespace IMS.Service.Inteface
{
   public interface ISellerService
    {
        List<SellerModel> GetSellerByOfficeId1(int id);

        List<SelectListItem> GetSellerByOfficeId(int id);

        List<SelectListItem> SellerLIst();

        int SelectByID(int id);

        int Insert(SellerModel obj);

        int Update(SellerModel obj);

        int Delete(int id);

        int Save();
    }
}
