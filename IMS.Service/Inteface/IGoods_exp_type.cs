﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using IMS.Service.Models;

namespace IMS.Service.Inteface
{
   public interface IGoods_exp_type
    {
        List<GoodExpTypeModel> GetAll();
        List<SelectListItem> GetSamanBargaList();
        int SelectByID(int id);
        int Insert(GoodExpTypeModel obj);
        int Update(GoodExpTypeModel obj);
        int Delete(int id);
        int Save();
    }
}
