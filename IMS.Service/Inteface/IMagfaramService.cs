﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using IMS.Service.Models;

namespace IMS.Service.Inteface
{
   public interface IMagfaramService
    {
        List<SelectListItem> GetAll();
       
        List<MagfaramModel> GetAllMag();
        List<SelectListItem> GetMagFaramIdByOfficeId(int id);
        int getMaxMagFaramNumberByOfficeId(int id);
        int SelectByID(int id);
        int Insert(MagfaramModel obj);
        int Update(MagfaramModel obj);
        int Delete(int id);
        int Save();
    }
}
