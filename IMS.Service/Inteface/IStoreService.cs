﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IMS.Service.Models;

namespace IMS.Service.Inteface
{
   public interface IStoreService
    {
       int Insert(StoreModel obj);
        int UpdateProductByOfficeIdProductId(int id1, int id2, int id3);
    }
}
