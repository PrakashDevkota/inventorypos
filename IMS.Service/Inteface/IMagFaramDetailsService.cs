﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IMS.Service.Models;

namespace IMS.Service.Inteface
{
   public interface IMagFaramDetailsService
    {
        List<MagFaramDetailsModel> GetAll();

        List<MagFaramDetailsModel> GetMagFaramByMagFaramId(int id);

        List<MagFaramDetailsModel> GetMagFaramDetailsByMagFaramId(int id);

        int Insert(MagFaramDetailsModel obj);

        int Save();
    }
}
