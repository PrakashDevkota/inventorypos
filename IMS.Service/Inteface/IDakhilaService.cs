﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IMS.Service.Models;

namespace IMS.Service.Inteface
{
   public interface IDakhilaService
    {
        List<DakhilaModel> GetAll();
      
        List<DakhilaModel> GetAllDakhila();

        int SelectByID(int id);

        int Insert(DakhilaModel obj);

        int Update(DakhilaModel obj);

        int Delete(int id);

        int Save();
    }
}
