﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using IMS.Service.Models;

namespace IMS.Service.Inteface
{
   public interface IProductDetailsService
    {
        List<ProductDetailsModel> GetAll();
        int Insert(ProductDetailsModel obj);
        int Update(ProductDetailsModel obj);
        List<SelectListItem> GetProductDetailsList();

        List<ProductDetailsModel> getProductDetailsByOfficeIdAndProductId(int id1, int id2);
        int Save();
        int SelectById(int id);
        int Delete(int id);
    }
}
