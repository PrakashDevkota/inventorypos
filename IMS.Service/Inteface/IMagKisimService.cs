﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IMS.Service.Models;

namespace IMS.Service.Inteface
{
   public interface IMagKisimService
    {
        List<MaagKisimModel> GetAll();
        int SelectByID(int id);
        int Insert(MaagKisimModel obj);
        int Update(MaagKisimModel obj);
        int Delete(int id);
        int Save();
    }
}
