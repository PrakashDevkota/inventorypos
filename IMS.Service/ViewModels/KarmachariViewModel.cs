﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using IMS.Service.Models;


namespace IMS.Service.ViewModels
{
   public class KarmachariViewModel
    {
        public KarmachariModel KarmachariModel { get; set; }

        public KarmachariDetailsModel KarmachariDetailsModel { get; set; }
            
        public List<SelectListItem> KarmachariList { get; set; }

        public FormAuthorityModel FormAuthorityModel { get; set; }
    }
}
