﻿using IMS.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace IMS.Service.ViewModels
{
  public  class ProductViewModel
    {
        public List<SelectListItem> CategoryList { get; set; }

        public List<ProductModel> ProductList { get; set; }

        public List<SelectListItem> ProductListItem { get; set; }

        public List<SelectListItem> SamanBargaList { get; set; }

        public List<SelectListItem> UnitList { get; set; }

        public ProductModel ProductModel { get; set; }

       

        public int Office_Id { get; set; }



    }
}
