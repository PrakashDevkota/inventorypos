﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IMS.Service.Models;

namespace IMS.Service.ViewModels
{
    public class KharidAdeshReportViewModel
    {
        public OfficeModel OfficeModel { get; set; }

        public DistrictModel DistrictModel { get; set; }

        public KharidAdeshModel KharidAdeshModel { get; set; }

        public KharidAdesDetailsModel KharidAdesDetailsModel { get; set; }

        public KarmachariModel KarmachariModel { get; set; }

        public KarmachariDetailsModel KarmachariDetailsModel { get; set; }

        public List<KharidAdesDetailsModel> kharidAdeshModelList { get; set; }
    }
}
