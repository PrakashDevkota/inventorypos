﻿using IMS.Service.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace IMS.Service.ViewModels
{
   public class KharidAdeshViewModel
    {
        public IEnumerable<KharidAdeshViewModel> KharidAdeshViewModela { get; set; }

        public List<SelectListItem> ProductList { get; set; }

        public List<SelectListItem> KharidPrakarList { get; set; }

        public List<KharidAdeshModel> KharidAdeshList { get; set; }

        public List<KharidAdesDetailsModel> KharidAdesDetailsModelList { get; set; }

        public List<SelectListItem> CategoryList { get; set; }


        public KharidAdeshModel KharidAdeshModel { get; set; }

        public CategoryModel CategoryModel { get; set; }

        public KharidAdesDetailsModel KharidAdesDetailsModel { get; set; }
    

        public List<SelectListItem> ProductDetailsList { get; set; }

        public ProductDetailsModel ProductDetailsModel { get; set; }


        public List<SelectListItem> KharidAdesNumberList { get; set; }
        [Required]
        public int Quantity { get; set; }

        public ProductModel ProductModel { get; set; }

       public List<KharidAdesDetailsModel> kharidAdeshModelList { get; set; }

        public int kharidAdesh_id { get; set; }


    }
}
