﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IMS.Service.Models;

namespace IMS.Service.ViewModels
{
   public class StoreViewModel
    {
        public int Office_Id { get; set; }
        public int Product_Id { get; set; }
        public NikasaModel NikasaModel { get; set; }
        public NikasaDetailsModel NikasaDetailsModel { get; set; }
    }
}
