﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using IMS.Service.Models;

namespace IMS.Service.ViewModels
{
   public class MagFaramViewModel
    {
        public List<SelectListItem> ProductList { get; set; }
        
        public List<SelectList> KarmachariList { get; set; }

        public MagfaramModel MagfaramModel { get; set; }

        public MagFaramDetailsModel MagFaramDetailsModel { get; set; }

        public List<SelectListItem> KarmachariDetailsModel { get; set; }

        public List<SelectListItem> CategoryList { get; set; }

        public List<SelectListItem> ProductDetailsList { get; set; }

        public ProductDetailsModel ProductDetailsModel { get; set; }

        public ProductModel ProductModel { get; set; }



    }
}
