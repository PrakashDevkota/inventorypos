﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using IMS.Service.Models;

namespace IMS.Service.ViewModels
{
   public class DakhilaKharidAdeshViewModel
    {
        public List<SelectListItem> ProductList { get; set; }

        public List<SelectListItem> KharidPrakarList { get; set; }

        public List<SelectListItem> KharidAdeshList { get; set; }

        public List<SelectListItem> QuantityList { get; set; }

        public List<SelectListItem> KharidAdeshDetailsList { get; set; }

        public List<SelectListItem> SellerLit { get; set; }

        public  DakhilaModel DakhilaModel { get; set; }

        public List<SelectListItem> sellerModelList { get; set; }
    }
}
