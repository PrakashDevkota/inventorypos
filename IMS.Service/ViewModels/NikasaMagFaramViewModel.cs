﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using IMS.Service.Models;

namespace IMS.Service.ViewModels
{
    public class NikasaMagFaramViewModel
    {
        public List<SelectListItem> ProductList { get; set; }

        public List<SelectListItem> KarmachariList { get; set; }

        public List<SelectListItem> MagFaramList { get; set; }

        public NikasaModel NikasaModel { get; set; }

        public NikasaDetailsModel NikasaDetailsModel { get; set;}
    }
}
