﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using IMS.Service.Models;

namespace IMS.Service.ViewModels
{
   public class SamanBargaKisimViewModel
    {
        public SamanKisimModel SamanKisimModel { get; set; }

        public GoodExpTypeModel GoodExpTypeModel { get; set; }

        public List<SelectListItem> SamanBargaList { get; set; }
    }
}
