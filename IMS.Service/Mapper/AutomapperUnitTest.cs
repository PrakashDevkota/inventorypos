﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IMS.Repository.Entities;
using IMS.Service.Models;

namespace IMS.Service.Mapper
{
   public class AutomapperUnitTest : AutoMapper.Profile
    {
        public static void Run()
        {
            AutoMapper.Mapper.Initialize(a =>
            {
                a.AddProfile<AutomapperUnitTest>();
            });
        }

        public AutomapperUnitTest()
        {
            CreateMap<unit, UnitModel>();
        }
    }
}
