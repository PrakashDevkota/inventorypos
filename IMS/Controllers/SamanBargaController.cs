﻿using System.Linq;using System;
using System.Collections.Generic;

using System.Web;
using System.Web.Mvc;
using IMS.Service.Inteface;
using IMS.Service.Service;
using IMS.Service.Models;

namespace IMS.Controllers
{
    public class SamanBargaController : Controller
    {
        // GET: SamanBarga
        private IGoods_exp_type samanBargaObj;
        public SamanBargaController()
        {
            this.samanBargaObj = new GoodExpTypeService();
        }

        public ActionResult Index()
        {
            List<GoodExpTypeModel> samanBargaLIst = samanBargaObj.GetAll();
            return View(samanBargaLIst);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(GoodExpTypeModel gm)
        {
            samanBargaObj.Insert(gm);
            samanBargaObj.Save();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var bargaList = samanBargaObj.GetAll().Find(x => x.Id.Equals(id));
            return View(bargaList);
        }

        [HttpPost]
        public ActionResult Edit(GoodExpTypeModel gm)
        {
            samanBargaObj.Update(gm);
            samanBargaObj.Save();
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            var samanBarga = samanBargaObj.Delete(id);
            samanBargaObj.Save();
            return RedirectToAction("Index");
        }
    }
}