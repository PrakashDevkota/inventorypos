﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IMS.Service.Inteface;
using IMS.Service.Service;
using IMS.Service.Models;

namespace IMS.Controllers
{
    public class KharidTypeController : Controller
    {
        // GET: KharidType
        private IKharidTypeService kharidObj;
        public KharidTypeController()
        {
            kharidObj = new KharidTypeService();

        }                                                                         
        public ActionResult Index()
        {
            List<KharidTypeModel> khariddesh = kharidObj.GetAll();
            return View(khariddesh);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(KharidTypeModel km)
        {
            kharidObj.Insert(km);
            kharidObj.Save();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var kharidTypeList = kharidObj.GetAll().Find(x => x.Id.Equals(id));
            return View(kharidTypeList);
        }

        [HttpPost]
        public ActionResult Edit(KharidTypeModel km)
        {
            kharidObj.Update(km);
            kharidObj.Save();
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            var kharidType = kharidObj.Delete(id);
            kharidObj.Save();
            return RedirectToAction("Index");
        }
    }
}