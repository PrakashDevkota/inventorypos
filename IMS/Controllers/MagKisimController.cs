﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IMS.Service.Inteface;
using IMS.Service.Service;
using IMS.Service.Models;

namespace IMS.Controllers
{
    public class MagKisimController : Controller
    {
        // GET: MagKisim
        private IMagKisimService magObj;
        public MagKisimController()
        {
            magObj = new MagKisimService();
        }
        public ActionResult Index()
        {
            List<MaagKisimModel> magkisim = magObj.GetAll();
            return View(magkisim);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(MaagKisimModel km)
        {
            magObj.Insert(km);
            magObj.Save();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var magkisim = magObj.GetAll().Find(x => x.Id.Equals(id));
            return View(magkisim);
        }

        [HttpPost]
        public ActionResult Edit(MaagKisimModel km)
        {
            magObj.Update(km);
            magObj.Save();
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
             magObj.Delete(id);
            magObj.Save();
            return RedirectToAction("Index");
        }
    }
}