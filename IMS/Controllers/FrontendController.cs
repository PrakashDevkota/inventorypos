﻿using IMS.Service.Inteface;
using IMS.Service.Models;
using IMS.Service.Service;
using IMS.Service.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IMS.Controllers
{
    public class FrontendController : Controller
    {
        private ICategoryService category;

        private IKharidAdesService kharidAdesObj = null;
        private IProductService productObj;
        private IKharidTypeService kharidTypeObj;
        private IKharidAdesDetailsService kharidDetailsObj;
        private ICategoryService categoryObj;
        private IProductDetailsService productDetailsObj;
        private IGoods_exp_type samanBargaObj;
        private ISellerService sellerObj;
        private IOfficeService officeObj;
        private IKarmachariDetailsService karmachariDetailsObj;
        private IMagfaramService magFaramObj;
       
       
        private IUnitService unitObj;

        public FrontendController()
        {
            this.category = new CategoryService();
            productObj = new ProductService();
            kharidAdesObj = new KharidAdesService();
            kharidTypeObj = new KharidTypeService();
            kharidDetailsObj = new KharidAdesDetailsService();
            categoryObj = new CategoryService();
            productDetailsObj = new ProductDetailsService();
            sellerObj = new SellerService();
            officeObj = new OfficeService();
            karmachariDetailsObj = new KarmachariDetailsService();
            magFaramObj = new MagfaramService();
            unitObj = new UnitService();
            samanBargaObj = new GoodExpTypeService();

        }
        // GET: Frontend
        public ActionResult Index()
        {
          
            OfficeModel OfficeName;
           
            if (Session["id"]!= null)
            {

               OfficeName =  officeObj.GetOfficeByUser(Convert.ToInt32(Session["id"].ToString()));
                Session["Office_Id"] = OfficeName.Id;
                Session["ProjectName"] = OfficeName.ProjectName;
                Session["OfficeNameNep"] = OfficeName.NameNepali;
                Session["local_level_code"] = OfficeName.LocalLevelCode;
                ViewBag.officename = OfficeName.NameNepali;
                return View("~/Views/Frontend/Index.cshtml");
            }
            return RedirectToAction("Index", "Account");

        }
       
        [HttpPost]
        public ActionResult KharidAdesh(FormCollection collection)
        {
            CategoryModel category1 = new CategoryModel();
            category1.Category_name = collection["email"];
           
            category1.Parent_id = 0;
            int letc = category.Insert(category1);

            return View();
        }

        public ActionResult KhadridAdesAddView()
        {
            if (Session["Office_Id"] != null)
            {

                KharidAdeshViewModel Record = new KharidAdeshViewModel();
                KharidAdeshModel kharidAdeshModel = new KharidAdeshModel();
                KharidAdesDetailsModel kharidAdeshDetailsModel = new KharidAdesDetailsModel();
                Record.KharidAdeshModel = kharidAdeshModel;
                Record.KharidAdesDetailsModel = kharidAdeshDetailsModel;
                Record.KharidAdeshModel.KharidAdesNumber = kharidAdesObj.getKharidAdeshNumberByOfficeId(Convert.ToInt32(Session["Office_Id"].ToString()));
                Record.CategoryList = categoryObj.GetCategorySelectListItem();
                //Record.ProductList = productObj.GetProductSelectListItem();
                Record.ProductDetailsList = productDetailsObj.GetProductDetailsList();
                Record.KharidPrakarList = kharidTypeObj.GetKharidPrakarSelectListItem();
                return PartialView("~/Views/Shared/Frontend/KharidAdes/_add.cshtml", Record);
            }
            else
            {
                return RedirectToAction("Index", "Account");
            }
        }
/// <summary>
///
/// </summary>
/// <returns></returns>
        public ActionResult KharidAdshEditView()
        {
            KharidAdeshViewModel Record = new KharidAdeshViewModel();
            if (Session["Office_Id"] != null)
            {
                List<KharidAdeshModel> kharidAdeshModel = new List<KharidAdeshModel>();
                kharidAdeshModel = kharidAdesObj.getAllKharidAdeshByOfficeId(Convert.ToInt32(Session["Office_Id"].ToString()));
                return PartialView("~/Views/Frontend/_kharidAdshEditView.cshtml", kharidAdeshModel);
            }
            else
            {
                return RedirectToAction("Index", "Account");
            }
        }

        public ActionResult KharidAdeshEditViewPage(int id)
        {
            KharidAdeshViewModel Record = new KharidAdeshViewModel();
            KharidAdeshModel kharidAdeshModel = new KharidAdeshModel();
            KharidAdesDetailsModel KharidAdesDetailsModel = new KharidAdesDetailsModel();
            //Record.KharidAdeshModel = kharidAdeshModel;
            Record.KharidAdesDetailsModel = KharidAdesDetailsModel;
            Record.KharidAdesDetailsModel.kharidAdeshModelList = kharidDetailsObj.getKharidAdeshDetailsByKharidAdeshIdAndOfficeId(id, Convert.ToInt32(Session["Office_Id"].ToString()));

            Session["kharidAdeshId"] = id;

            Record.CategoryList = categoryObj.GetCategorySelectListItem();
            Record.ProductList = productObj.GetProductSelectListItem();
            Record.ProductDetailsList = productDetailsObj.GetProductDetailsList();
            Record.KharidPrakarList = kharidTypeObj.GetKharidPrakarSelectListItem();
            return PartialView("~/Views/Shared/Frontend/KharidAdes/_kharidAdeshEditPage.cshtml", Record);
        }

        public ActionResult DakhilaAddView()
        {
            DakhilaKharidAdeshViewModel Record = new DakhilaKharidAdeshViewModel();
            DakhilaModel dakhilaModel = new DakhilaModel();
            if (Session["id"] == null)
            {
                return RedirectToAction("Index", "Account");
            }
            if (Session["id"] != null )
            {

                Record.KharidAdeshList = kharidAdesObj.GetKharidListItemsByOfficId(Convert.ToInt32(Session["id"].ToString()));
                //Record.KharidAdeshDetailsList = kharidDetailsObj.GetKharidAdesDetailsList();
                Record.ProductList = productObj.GetProductSelectListItem();
                Record.sellerModelList = sellerObj.GetSellerByOfficeId(Convert.ToInt32(Session["Office_Id"].ToString()));
            }
            return PartialView("~/Views/Shared/Frontend/Dakhila/_dakhilaAdd.cshtml",Record);
        }

        public ActionResult MagFaram()
        {

            MagFaramViewModel Record = new MagFaramViewModel();
            MagfaramModel MagfaramModel = new MagfaramModel();
            Record.MagfaramModel = MagfaramModel;
            Record.MagfaramModel.Mag_faram_number = magFaramObj.getMaxMagFaramNumberByOfficeId(Convert.ToInt32(Session["Office_Id"].ToString()));
            Record.KarmachariDetailsModel = karmachariDetailsObj.GetKarmachariByOfficeId((Session["Office_Id"].ToString()));
            Record.CategoryList = categoryObj.GetCategorySelectListItem();
            
            Record.ProductDetailsList = productDetailsObj.GetProductDetailsList();
            Record.ProductList = productObj.GetProductSelectListItem();
            return PartialView("~/Views/Shared/Frontend/MagFaram/_magFaram.cshtml", Record);
        }

        public ActionResult Nikasa()
        {
            NikasaMagFaramViewModel Record = new NikasaMagFaramViewModel();
            Record.MagFaramList = magFaramObj.GetMagFaramIdByOfficeId(Convert.ToInt32(Session["id"].ToString()));
            return PartialView("~/Views/Shared/Frontend/Nikasa/_nikasaAddView.cshtml",Record);
        }

        public ActionResult KharidAdeshReportView()
        {
            
            KharidAdesDetailsModel Record = new KharidAdesDetailsModel();
            Record.KharidAdeshListItem = kharidAdesObj.GetKharidListItemsByOfficId(Convert.ToInt32(Session["id"].ToString()));
           
            return PartialView("~/Views/Shared/Frontend/KharidAdes/_kharidAdeshReport.cshtml", Record);
        }

       public ActionResult MagFaramReportView()
        {
            MagFaramDetailsModel Record = new MagFaramDetailsModel();
            Record.MagFaramList = magFaramObj.GetMagFaramIdByOfficeId(Convert.ToInt32(Session["id"].ToString()));
            return PartialView("~/Views/Shared/Frontend/MagFaram/_magFaramReport.cshtml", Record);
        }

        public ActionResult KarmachariView()
        {

            return PartialView("~/Views/Shared/Frontend/Karmachari/_karmachari.cshtml");

        }

        public ActionResult SellerView()
        {

            return PartialView("~/Views/Shared/Frontend/Seller/_seller.cshtml");
        }

        public ActionResult FormAuthorityView()
        {
            FormAuthorityModel Record = new FormAuthorityModel();
             Record.KarmachariList = karmachariDetailsObj.getKarmachariDetailsByOfficeId(Convert.ToInt32(Session["Office_Id"].ToString()));
            return PartialView("~/Views/Shared/Frontend/FormAuthority/_formAuthority.cshtml", Record);
        }

        public ActionResult ProductAddView()
        {
            ProductViewModel Record = new ProductViewModel();
            Record.ProductList = productObj.GetAll();
            Record.CategoryList = categoryObj.GetCategorySelectListItem();
            Record.UnitList = unitObj.UnitSelectItem();
            Record.SamanBargaList = samanBargaObj.GetSamanBargaList();
            return PartialView("~/Views/Shared/Frontend/Product/_productAddView.cshtml", Record);
        }

        public ActionResult ProductDetailsAddView()
        {
            ProductDetailsModel Record = new ProductDetailsModel();
            Record.ProductList = productObj.GetProductSelectListItem();
            return PartialView("~/Views/Shared/Frontend/Product/_productDetailsAddView.cshtml",Record);
        }

       

        public JsonResult GetKhairdAdeshDetailsByOfficeId(int kharid_aadesh_id)
        {
            List<KharidAdesDetailsModel> model = kharidDetailsObj.getKharidAdeshDetailsByKharidAdeshIdAndOfficeId(kharid_aadesh_id, Convert.ToInt32(Session["Office_Id"].ToString()));
            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}
