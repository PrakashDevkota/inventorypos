﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IMS.Service.Inteface;
using IMS.Service.Service;
using IMS.Service.Models;
using IMS.Service.ViewModels;

namespace IMS.Controllers
{
    public class KarmachariDetailsController : Controller
    {
        // GET: KarmachariDetails
        private IKarmachariService karmachariObj;
        private IKarmachariDetailsService karmachariDetailsObj;

        public KarmachariDetailsController()
        {
            this.karmachariDetailsObj = new KarmachariDetailsService();
            this.karmachariObj = new KarmachariService();
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create(KarmachariViewModel kd)
        {
            KarmachariModel km = kd.KarmachariModel;
            KarmachariDetailsModel kmd = kd.KarmachariDetailsModel;
            int Id =   karmachariObj.Insert(km);

            karmachariDetailsObj.insert(kmd, Id);
            return RedirectToAction("Index", "Frontend");
        }


        // this function help to save form authority in form_authority table
        public ActionResult FormAuthorityCreate(FormAuthorityModel kv)
        {

            karmachariDetailsObj.InsertFormAuthority(kv);
           
            return RedirectToAction("Index", "Frontend");
        }
    }
}