﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IMS.Service.Models;
using IMS.Service.Inteface;
using IMS.Service.Service;

namespace IMS.Controllers
{
    public class DakhilaController : Controller
    {
        // GET: Dakhila
        private IDakhilaService dakhilaObj;
        private IDakhilaDetailsService dakhilaDetailsObj;
        private IStoreService storeObj;
        public DakhilaController()
        {
            this.dakhilaObj = new DakhilaService();
            this.dakhilaDetailsObj = new DakhilaDetailsService();
            this.storeObj = new StoreService();
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(DakhilaModel dm)
        {
            return RedirectToAction("Index");
        }

        public JsonResult SaveDakhilaDetails(DakhilaDetailsModel[] dakhilaDetails, DakhilaModel dakhila, StoreModel[] store)
        {
            int temp = dakhilaObj.Insert(dakhila);
            foreach (var item in dakhilaDetails)
            {

                item.dakhila_id = temp;
                dakhilaDetailsObj.Insert(item);
            }
            foreach (var item in store)
            {
                storeObj.Insert(item);
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}