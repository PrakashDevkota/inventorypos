﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IMS.Repository.Entities;
using IMS.Service.Models;
using IMS.Service.Inteface;
using IMS.Service.Service;



namespace IMS.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        private IUserService userObj;
        public AccountController()
        {
            this.userObj = new UserService(); 
        }
        
        public ActionResult Index()

        {
            return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }
       
        [HttpPost]
        public ActionResult Login(UserModel user)
        {
                user currentUser = userObj.CheckUserLogin(user);
                if(currentUser != null)
                {
                    Session["id"]= currentUser.id;
                    
                    Session["username"]= currentUser.username;
                    
                    return RedirectToAction("Index", "Frontend");
            
                }
                //else
                //{
                //Session.Add("error", "Username or password does not match.");   
               
                //    ModelState.AddModelError("", "Username or password does not match.");
                //}
            return RedirectToAction("Index", "Account");

        }

        public ActionResult Logout()
        {
            Session["id"] = null;
            Session["username"] = null;
            Session.Remove("id");
            Session.Remove("username");
            return RedirectToAction("Index", "Account");
        }


    }
}