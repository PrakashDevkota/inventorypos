﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IMS.Service.Models;
using IMS.Service.Inteface;
using IMS.Service.Service;

namespace IMS.Controllers
{
    public class MagfaramController : Controller
    {
        // GET: Magfaram

        private IMagfaramService magfaramObj;
        private IMagFaramDetailsService magFaramDetailsObj;
        public MagfaramController()
        {
            this.magfaramObj = new MagfaramService();
            this.magFaramDetailsObj = new MagFaramDetailsService();
        }
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(MagfaramModel mm)
        {
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            return View();
        }

        [HttpPost]
        public ActionResult Edit(MagfaramModel mm)
        {
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            return RedirectToAction("Index");
        }


        // Save Mag Faramand and MagFaram Details From Mag faram View
        public JsonResult SaveFromFront(MagfaramModel magFaram, MagFaramDetailsModel[] magFaramDetails)
        {
           int temp = magfaramObj.Insert(magFaram);
            foreach (var item in magFaramDetails)
            {
                item.Mag_Faram_Id = temp;

                magFaramDetailsObj.Insert(item);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        // Fetch Mag Faram Id Called from Nikasa View

        public JsonResult GetMagFaramById(int id)
        {
            List<MagFaramDetailsModel> data = magFaramDetailsObj.GetMagFaramByMagFaramId(id);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

      
    }
}