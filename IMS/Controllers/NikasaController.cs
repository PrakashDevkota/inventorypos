﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IMS.Service.Models;
using IMS.Service.Inteface;
using IMS.Service.Service;
using IMS.Service.ViewModels;

namespace IMS.Controllers
{
    public class NikasaController : Controller
    {
        // GET: Nikasa
        private INikasaService nikasaObj;
        private INikasaDetailsService nikasaDetailsObj;
        private IStoreService storeObj;
        public NikasaController()
        {
            this.nikasaObj = new NikasaService();
            this.nikasaDetailsObj = new NikasaDetailsService();
            this.storeObj = new StoreService();
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        public JsonResult SaveFromFront(NikasaModel Nikasa, NikasaDetailsModel[] NikasaDetails, StoreModel[] Store)
        {

            int temp = nikasaObj.Insert(Nikasa);
          

            foreach (var item in NikasaDetails)
            {
                item.Nikasa_Id = temp;
                
                nikasaDetailsObj.Insert(item);
               
            }

            foreach (var item in Store)
            {
                int product_id = item.Product_Id;
                int office_id = item.Office_Id;
                int quantity = item.Quantity;
                storeObj.UpdateProductByOfficeIdProductId(product_id, office_id, quantity);

            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}