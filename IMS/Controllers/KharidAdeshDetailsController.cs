﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IMS.Service.Models;
using IMS.Service.Inteface;
using IMS.Service.Service;
using IMS.Service.ViewModels;

namespace IMS.Controllers
{
    public class KharidAdeshDetailsController : Controller
    {
        // GET: KharidAdeshDetails
        private IKharidAdesDetailsService kharidDetailsObj;
        private ISellerService sellerObj;
        private IDakhilaService dakhilaObj;
        private IDakhilaDetailsService dakhilaDetailsObj;
        private IStoreService storeObj;
        private IOfficeService officeObj;
        public KharidAdeshDetailsController()
        {
            this.kharidDetailsObj = new KharidAdesDetailsService();
            this.sellerObj = new SellerService();
            this.dakhilaObj = new DakhilaService();
            this.dakhilaDetailsObj = new DakhilaDetailsService();
            this.storeObj = new StoreService();
            this.officeObj = new OfficeService();
        }
        public ActionResult Index()
        {
            var KharidAdesDetailsModel = new KharidAdesDetailsModel();
            return View(KharidAdesDetailsModel);
        }

        public JsonResult List(int id)
        {
            List<KharidAdesDetailsModel> data = kharidDetailsObj.GetByKharidAdeshId(id);
            List<SellerModel> sellers = sellerObj.GetSellerByOfficeId1(Convert.ToInt32(Session["id"].ToString()));
            var result = new { Data = data, Sellers = sellers };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult KharidAdesDetailForDakhila(DakhilaDetailsModel [] dakhilaDetails, DakhilaModel dakhila, StoreModel[] store)
        {
           int temp =  dakhilaObj.Insert(dakhila);
            foreach (var item in dakhilaDetails)
            {

                item.dakhila_id = temp;
                dakhilaDetailsObj.Insert(item);
            }
            foreach (var item in store)
            {
                storeObj.Insert(item);
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult KharidAdeshReport(KharidAdesDetailsModel ka)
        {
            OfficeModel OfficeName;
                OfficeName = officeObj.GetOfficeByUser(Convert.ToInt32(Session["id"].ToString()));
                Session["Office_Id"] = OfficeName.Id;
                Session["ProjectName"] = OfficeName.ProjectName;
                Session["OfficeNameNep"] = OfficeName.NameNepali;
            Session["District_Id"] = OfficeName.DistrictId;
            Session["District_name"] = OfficeName.District_Name;
            Session["Local_Level_Type_Name"] = OfficeName.Local_Level_Type_Nmae;
                
               
            
            int khaid_adesh_id = ka.Kharid_ades_id;
            KharidAdeshReportViewModel Record = new KharidAdeshReportViewModel();
            
           List<KharidAdesDetailsModel> kharidAdeshModelList = kharidDetailsObj.GetKharidAdeshDetailsByKharidAdeshId(khaid_adesh_id);
            //Record.DistrictModel =  
            return View(kharidAdeshModelList);
        }
    }
}