﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IMS.Service.Inteface;
using IMS.Service.Service;
using IMS.Service.Models;

namespace IMS.Controllers
{
    public class SellerController : Controller
    {
        // GET: Seller
        private ISellerService selleObj;
        public SellerController()
        {
            selleObj = new SellerService();
        }
        public ActionResult Index()
        {
            //List<SellerModel> khariddesh = selleObj.GetSellerByOfficeId();
            //return View(khariddesh);
            return View();
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(SellerModel km)
        {
            selleObj.Insert(km);
            selleObj.Save();
            return RedirectToAction("Index","Frontend");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            //var kharidTypeList = selleObj.GetAll().Find(x => x.Id.Equals(id));
            return View();
        }

        [HttpPost]
        public ActionResult Edit(SellerModel km)
        {
            selleObj.Update(km);
            selleObj.Save();
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            var kharidType = selleObj.Delete(id);
            selleObj.Save();
            return RedirectToAction("Index");
        }
    }
}