﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IMS.Service.Inteface;
using IMS.Service.Service;
using IMS.Service.Models;
using IMS.Repository.Entities;

namespace IMS.Controllers
{
    public class ProductDetailsController : Controller
    {
        // GET: ProductDetails
        llimsEntities db;
        private IProductDetailsService productDetailsObj = null;
        public ProductDetailsController()
        {
            productDetailsObj = new ProductDetailsService();
            this.db = new llimsEntities();
        } 

        public ActionResult Index()
        {
            var EntityRecord = productDetailsObj.GetAll();
            return View(EntityRecord);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(ProductDetailsModel pm)
        {
            productDetailsObj.Insert(pm);
            productDetailsObj.Save();
            return RedirectToAction("Index","Frontend");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var EnitityRecord = productDetailsObj.GetAll().Where(p => p.Id == id).FirstOrDefault();
            return View(EnitityRecord);
        }

        [HttpPost]
        public ActionResult Edit(ProductDetailsModel pm)
        {
            productDetailsObj.Update(pm);
            productDetailsObj.Save();
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            productDetailsObj.Delete(id);
            productDetailsObj.Save();
            return RedirectToAction("Index");
        }

        public JsonResult getProductDetailsByProductId(int office_id, int product_id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            List<ProductDetailsModel> data = productDetailsObj.getProductDetailsByOfficeIdAndProductId(office_id, product_id);
            return Json(data, JsonRequestBehavior.AllowGet);
            
        }
    }
}