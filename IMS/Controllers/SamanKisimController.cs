﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IMS.Service.Inteface;
using IMS.Service.Models;
using IMS.Service.Service;
using IMS.Service.ViewModels;

namespace IMS.Controllers
{
    public class SamanKisimController : Controller
    {
        // GET: SamanKisim
        private ISamanKisimService samanKisimObj;
        private IGoods_exp_type samanBargaObj;
        public SamanKisimController()
        {
            this.samanKisimObj = new SamanKisimService();
            this.samanBargaObj = new GoodExpTypeService();
        }
        public ActionResult Index()
        {
            var samanKisimList = samanKisimObj.GetAll();
            return View(samanKisimList);
        }

        [HttpGet]
        public ActionResult Create()
        {
            SamanBargaKisimViewModel Record = new SamanBargaKisimViewModel();
           Record.SamanBargaList = samanBargaObj.GetSamanBargaList();
            return View(Record);
        }

        [HttpPost]
        public ActionResult Create(SamanBargaKisimViewModel sk)
        {
            SamanKisimModel data = sk.SamanKisimModel;
           
                samanKisimObj.Insert(data);
    
            
            samanKisimObj.Save();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var samanKisimList = samanKisimObj.GetAll().Find(x => x.Id == id);
            return View(samanKisimList);
        }

        [HttpPost]
        public ActionResult Edit(SamanKisimModel sk)
        {
            samanKisimObj.Update(sk);
            samanKisimObj.Save();
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            samanKisimObj.Delete(id);
            samanKisimObj.Save();
            return RedirectToAction("Index");
        }
    }
}