﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IMS.Service.Models;
using IMS.Service.Inteface;
using IMS.Service.Service;

namespace IMS.Controllers
{
    public class MagFaramDetailsController : Controller
    {
        // GET: MagFaramDetails
        private IMagFaramDetailsService magFaramDetailsObj;
        public MagFaramDetailsController()
        {
            this.magFaramDetailsObj = new MagFaramDetailsService();
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult MagFaramReportView(MagFaramDetailsModel md)
        {
            int mag_faram_id = md.Mag_Faram_Id;
            List<MagFaramDetailsModel> data = magFaramDetailsObj.GetMagFaramDetailsByMagFaramId(mag_faram_id);

            return View();
        }
    }
}