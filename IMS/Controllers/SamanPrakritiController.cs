﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IMS.Service.Inteface;
using IMS.Service.Models;
using IMS.Service.Service;

namespace IMS.Controllers
{
    public class SamanPrakritiController : Controller
    {
        // GET: SamanPrakriti
        private ISamankoPrakritiService samaPrakritiObj;
        public SamanPrakritiController()
        {
            this.samaPrakritiObj = new SamankoPrakritiService();
        }
        public ActionResult Index()
        {
            var samanList = samaPrakritiObj.GetAll();
            return View(samanList);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(SamankoPrakritiModel sm)
        {
            samaPrakritiObj.Insert(sm);
            samaPrakritiObj.Save();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var samaPrakritiList = samaPrakritiObj.GetAll().Find(x => x.Id.Equals(id));
            return View(samaPrakritiList);
        }

        [HttpPost]
        public ActionResult Edit(SamankoPrakritiModel km)
        {
            samaPrakritiObj.Update(km);
            samaPrakritiObj.Save();
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            var kharidType = samaPrakritiObj.Delete(id);
            samaPrakritiObj.Save();
            return RedirectToAction("Index");
        }
    }
}