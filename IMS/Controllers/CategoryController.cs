﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IMS.Service.Inteface;
using IMS.Service.Service;
using IMS.Service.Models;

namespace IMS.Controllers
{
    public class CategoryController : Controller
    {
        // GET: Category

        private ICategoryService catObj = null;
        public CategoryController()
        {
            catObj = new CategoryService();
        }
        public ActionResult Index()
        {
            var catList = catObj.GetAll();
            return View(catList);
        }

        [HttpGet]
        public ActionResult Create()
        {
           
            return View();
        }

        [HttpPost]
        public ActionResult Create(CategoryModel cm)
        {
            catObj.Insert(cm);
            catObj.Save();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var catList = catObj.GetAll().Find(x => x.Id.Equals(id));
            return View(catList);
        }

        [HttpPost]
        public ActionResult Edit(CategoryModel cm)
        {
            catObj.Update(cm);
            catObj.Save();
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            catObj.Delete(id);
            catObj.Save();
            return RedirectToAction("Index");
        }
    }
}