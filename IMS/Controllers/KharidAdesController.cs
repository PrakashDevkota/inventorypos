﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IMS.Service.Inteface;
using IMS.Service.Service;
using IMS.Service.Models;
using IMS.Repository.Entities;
using IMS.Service.ViewModels;

namespace IMS.Controllers
{
    public class KharidAdesController : Controller
    {
        // GET: KharidAdes
        private IKharidAdesService kharidAdesObj = null;
        private IProductService productObj;
        private IKharidTypeService kharidTypeObj;
        private IKharidAdesDetailsService kharidDetailsObj;
        private IStoreService storeObj;
        public KharidAdesController()
        {
            productObj = new ProductService();
            kharidAdesObj = new KharidAdesService();
            kharidTypeObj = new KharidTypeService();
            kharidDetailsObj = new KharidAdesDetailsService();
            storeObj = new StoreService();
                
                }
        public ActionResult Index()
        {
           // ViewBag.modelRecord = kharidAdesObj.GetAll();
           
            KharidAdeshViewModel Record = new KharidAdeshViewModel();
            //KharidAdeshModel kharidAdeshModel = new KharidAdeshModel();
           
            //Record.KharidAdeshModel = kharidAdeshModel;
            Record.KharidAdeshList = kharidAdesObj.GetKharidListItem();
            return View(Record);
        }

        [HttpGet]
        public ActionResult Create()
        {
            KharidAdeshViewModel Record = new KharidAdeshViewModel();
            KharidAdeshModel kharidAdeshModel = new KharidAdeshModel();
            KharidAdesDetailsModel kharidAdeshDetailsModel = new KharidAdesDetailsModel();
            Record.KharidAdeshModel = kharidAdeshModel;
            Record.KharidAdesDetailsModel = kharidAdeshDetailsModel;
            Record.ProductList = productObj.GetProductSelectListItem();
            Record.KharidPrakarList = kharidTypeObj.GetKharidPrakarSelectListItem();


            return View(Record);
        }

        [HttpPost]
        public ActionResult Create(KharidAdeshViewModel km)
        {
            
            var kharidAdesh = new KharidAdeshModel()
            {
                KharidAdesNumber = km.KharidAdeshModel.KharidAdesNumber,
                CreatedDate = km.KharidAdeshModel.CreatedDate,
                Status = 1,
                Office_id = "1"
            };
            kharidAdesObj.Insert(kharidAdesh);

            var kharidAdesDetails = new KharidAdesDetailsModel()
            {
                Kharid_ades_id = km.KharidAdeshModel.KharidAdesNumber,
                Product_id = km.KharidAdeshModel.product_id,
                Kharid_type_id = km.KharidAdesDetailsModel.Kharid_type_id,
                Quantity = km.KharidAdesDetailsModel.Quantity,

            };


            kharidDetailsObj.Insert(kharidAdesDetails);

            return RedirectToAction("Index");

        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            ViewBag.productList = productObj.GetAllProduct();
            ViewBag.modelRecord = kharidAdesObj.GetAllKharidAdes().Find(p => p.Id.Equals(id));
            return View();
        }

        [HttpPost]
        public ActionResult Edit(KharidAdeshModel km)
        {
            kharidAdesObj.Update(km);
            kharidAdesObj.Save();
            return RedirectToAction("Index");

        }

        public ActionResult Delete(int id)
        {
            kharidAdesObj.Delete(id);
            kharidAdesObj.Save();
            return RedirectToAction("Index");
        }



        public JsonResult SaveFromFront(KharidAdeshModel KharidAdes, KharidAdesDetailsModel [] KharidAdesDetails)
        {
            
           int temp = kharidAdesObj.Insert(KharidAdes);   
             
            foreach (var item in KharidAdesDetails)
            {
               item.Kharid_ades_id = temp;
             
                kharidDetailsObj.Insert(item);
            }
           
            
            return Json(true, JsonRequestBehavior.AllowGet);
        }

       
    }
}