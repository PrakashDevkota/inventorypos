﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IMS.Service.Inteface;
using IMS.Service.Service;
using IMS.Service.Models;
using System.Dynamic;
using IMS.Service.ViewModels;
using IMS.Repository.Entities;

namespace IMS.Controllers
{
    public class ProductController : Controller
    {
        // GET: Product
        private ICategoryService categoryObj = null;
        private IProductService productObj;
        private ISamanKisimService samanKisimObj;
        private ISamankoPrakritiService samanPrakritiObj;
        private IUnitService unitObj;
        llimsEntities db;
        public ProductController()
        {
            categoryObj = new CategoryService();
            productObj = new ProductService();
            samanKisimObj = new SamanKisimService();
            samanPrakritiObj = new SamankoPrakritiService();
            unitObj = new UnitService();
            this.db = new llimsEntities();
        }
        public ActionResult Index()
        {

            List<ProductModel> ProductList = new List<ProductModel>();
            ProductList = productObj.GetAll();
            return View(ProductList);
        }



        [HttpGet]
        public ActionResult Create()
        {
            ProductViewModel Record = new ProductViewModel();
            ProductModel productModel = new ProductModel();
            Record.ProductModel = productModel;
            Record.UnitList = unitObj.UnitSelectItem();
            Record.CategoryList = categoryObj.GetCategorySelectListItem();
            //Record.SamanKisimList = samanKisimObj.GetSamanKisimSelectListItem();
            //Record.SamanKisimList = samanKisimObj.GetAll();
            //Record.SamanPrakritiList = samanPrakritiObj.GetSamanPrakritiSelectListItem();
            return View(Record);
        }

        [HttpPost]
        public ActionResult Create(ProductViewModel pm)
        {

            productObj.Insert(pm.ProductModel);
            productObj.Save();
            return RedirectToAction("Index","Frontend");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var productList = productObj.GetAll().Find(p => p.Id == id);
            return View(productList);
        }

        [HttpPost]
        public ActionResult Edit(ProductModel pm)
        {
            productObj.Update(pm);
            productObj.Save();
            return RedirectToAction("Index");
        }

        // drop down casecading depending on category Id and Office id
       
        public JsonResult GetProductByCategoryIdOfficeId(int category_id, int office_id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            List<ProductModel> productList = productObj.getProductByOfficeIdAndCategoryId(category_id, office_id);
            return Json(productList, JsonRequestBehavior.AllowGet);
        }
    }
}