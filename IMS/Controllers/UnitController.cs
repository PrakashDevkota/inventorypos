﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IMS.Repository.Entities;
using IMS.Service.Inteface;
using IMS.Service.Models;
using IMS.Service.Service;


namespace IMS.Controllers
{
    public class UnitController : Controller
    {
        // GET: Unit
        private IUnitService unitObj;
        public UnitController()
        {
            this.unitObj = new UnitService();
        }


        public ActionResult Index()
        {
            List<UnitModel> unitList = unitObj.GetAll();
            return View(unitList);
        }
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(UnitModel u)
        {
            unitObj.Insert(u);
            unitObj.Save();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var list = unitObj.GetAll().Find(x=>x.Id.Equals(id));
            return View(list);
        }

        [HttpPost]
        public ActionResult Edit(UnitModel u)
        {
            unitObj.Update(u);
            unitObj.Save();
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            var unit = unitObj.Delete(id);
            unitObj.Save();
            return RedirectToAction("Index");
        }
    }
}