﻿/*
 * Kharid Adesh form submit
 * Source : _add.chtml
 */
var i = 1;
var KharidAdesDetails = [];

function KharidAdeshFormSubmit(event) {
    //var form = $('#kharid_ades_form');
    //$.validator.unobtrusive.parse(form);
    event.preventDefault();
    var KharidAdesNumber = $('#KharidAdeshModel_KharidAdesNumber').val();
    var ProductId = $('#ProductModel_Id option:selected').text();
    var KharidType = $('#KharidAdesDetailsModel_Kharid_type_id option:selected').text();
    var ProductDetailsSpecification = $('#ProductDetailsModel_Id option:selected').text();
    var CategoryId = $('#KharidAdeshModel_Category_Id option:selected').text();
    var productDetailsId = $('#ProductDetailsModel_Id option:selected').text();
    
    var Quantity = $('#KharidAdesDetailsModel_Quantity').val();
    var CategoryN = $('#KharidAdeshModel_Category_Id').val();
    var ProductIdN = $('#ProductModel_Id').val();
    var KharidTypeN = $('#KharidAdesDetailsModel_Kharid_type_id').val();
    var ProductDetailsId = $('#ProductDetailsModel_Id').val();
   
    var userId = $('#myHiddenVar').val();
    var OfficeId = $('#officeId').val();
   // var Created_Date = $('#nepali_date');

   
    //console.log(ProductDetailsId);
   
    KharidAdesDetails.push({
        //Kharid_ades_id: Id,
        Category_Id: CategoryN,
        product_id: ProductIdN,
        Quantity: Quantity,
        Kharid_type_id: KharidTypeN,
        Product_Details_Id:  ProductDetailsId
         

    });
    //console.log(KharidType);
   // console.log(KharidAdesDetails);
    var tr = "<tr>" +
        "<td>" +
        i +
        "</td>" +
        "<td class='category_id'>" +
        CategoryId +
        "</td>" +
        "<td class='product_id'>" +
        ProductId +
        "</td>" +
        "<td class='product_details_id'>" +
        productDetailsId +
        "</td>" +
        "<td class='kharid_type_id'>" +
        KharidType +
        "</td>" +
        "<td class='quantity'>" +
        Quantity +
        "</td>" +
        "<td>" +
        '<a href="#" onclick="getData()">Edit</a> | <a href="#" onclick="Delete()">Delete</a>'  +
        "</td>";
    i = i + 1;       
    $('#kharid_ades_table').find('tbody').last('tr').append(tr);
    $('#KharidAdeshDetailModel_Quantity').val('');
    clearTextBoxForKharidAdesh();
   
}





/*
 * Kharid Adesh and Kharid Ades Details save
 * source : _add.chtml
 *  
 */

function save() {
    //alert('lets go...');
    var CategoryId = $('#KharidAdeshModel_Category_Id').val();
    var ProductIdN = $('#KharidAdeshModel_product_id').val();
    var SamankoKisimN = $('#KharidAdeshDetailModel_Kharid_type_id').val();
    var KharidAdesNumber = $('#KharidAdeshModel_KharidAdesNumber').val();
    
    var Quantity = $('#KharidAdeshDetailModel_Quantity').val();
    var userId = $('#myHiddenVar').val();
    var OfficeId = $('#officeId').val();
    var Created_Date = $('#nepali_date').val();


    var KharidAdes = [];
    KharidAdes = {
        KharidAdesNumber: KharidAdesNumber,
        Office_id: OfficeId,
        Created_by: userId,
        CreatedDate: Created_Date
    };

    //console.log(Created_Date);
    console.log(KharidAdesDetails);



    $.ajax({
        url: MyAppUrlSettings.KharidAdesURL,
        type: "POST",
        data: {
            KharidAdes: KharidAdes,
            KharidAdesDetails: KharidAdesDetails,
           
        },
        datatype: 'json',
        ContentType: 'application/json;utf-8',
        success: function (resp) {
            $('.flash-message').fadeIn("fast");
            setTimeout(function () {
                $('.flash-message').fadeOut("slow");
            }, 1000)
            //alert('Success ' + resp);
            //console.log(resp)

        },
        error: function (err) {
            alert("Error " + err.status);
        }
    });
    $("#kharid_ades_table tbody tr").remove();
}

/*
 * 
 
 *  *** Dakhila start ****
  
 */



function saveDakhila() {
    //alert('lets go dakhila...');
    var trs = $('#dakhila-table').find('tbody tr:not(:first)');
    var DakhilaDetails = [];
    var store = [];
    var Karid_adesh_id = $('#kharid_adesh_number').val();
    var Created_Date = $('#nepali_date').val();
    var Office_Id = $('#officeId').val();
    var Created_By = $('#myHiddenVar').val();
    var Dakhila;
    Dakhila = {
        Kharid_adesh_id: Karid_adesh_id,
        Created_by: Created_By,
        Office_id: Office_Id,
        Created_date: Created_Date
    };

    //console.log(Created_Date);
    $.each(trs, function (key, value) {

        var category_id = $(value).find('.category_id').text();
        var product_id = $(value).find('.product_id').text();
        var product_details_id = $(value).find('.product_details_id').text();
        var kharid_type_id = $(value).find('.kharid_type_id').text();
        var quantity = $(value).find('.quantity').text();
        var seller = $(value).find('.seller option:selected').val();


        DakhilaDetails.push({
            Category_Id: category_id,
            Product_id: product_id,
            Product_Details_Id: product_details_id,
            Kharid_adesh_id: Karid_adesh_id,
            Kharid_type_id: kharid_type_id,
            Quantity: quantity,
            Seller: seller

        });
        store.push({
            product_id: product_id,
            Kharid_type_id: kharid_type_id,
            Quantity: quantity,
            Office_Id: Office_Id,
            Created_By: Created_By,
            Created_Date: Created_Date
        });
        console.log(DakhilaDetails);


    });


    $.ajax({
        url: MyAppUrlSettings.DakhilaDetailsURL, // _dakhilaAdd.cshtml mag
        type: "POST",
        data: {
            dakhilaDetails: DakhilaDetails,
            dakhila: Dakhila,
            store: store

        },
        datatype: 'json',
        ContentType: 'application/json;utf-8',
        success: function (resp) {
            $('.flash-message').fadeIn("fast");
            setTimeout(function () {
                $('.flash-message').fadeOut("slow");
            }, 1000)
            //alert('Success ' + resp);
            console.log(resp)

        },
        error: function (err) {
            alert("Error " + err.status);
        }
    });
    //$("#kharid_ades_table tbody tr").remove();
}

/*
 * 
       Mag Faram Strart
 *
*/


var magFaramDetails = [];
function MagFaramformSubmit(event) {
    //alert('hunter');
    event.preventDefault();
      var magFaramNumber = $('#MagfaramModel_Mag_faram_number').val();
    var Request_By = $('#MagFaramDetailsModel_Request_By option:selected').text();
    var Category_Id = $('#MagFaramDetailsModel_Category_Id option:selected').text();
    var Product_id = $('#ProductModel_Id option:selected').text();
    var Product_Details = $('#ProductDetailsModel_Id').text();
   
    var Quantity = $('#MagFaramDetailsModel_Quantity').val();

    var RequestByN = $('#MagFaramDetailsModel_Request_By').val();
    var ProductIdN = $('#ProductModel_Id').val();
    var CategoryIdN = $('#MagFaramDetailsModel_Category_Id').val();
    var ProductDetailsIdN = $('#ProductDetailsModel_Id').val();
    var userId = $('#myHiddenVar').val();
    var OfficeId = $('#officeId').val();
    //alert(magFaramNumber);

     console.log(Product_id);
    // console.log(KharidAdesNumber);

    magFaramDetails.push({

        // mag_faram_id: Id,
        category_id: CategoryIdN,
        request_by: RequestByN,
        product_id: ProductIdN,
        product_details_id: ProductDetailsIdN,
        quantity: Quantity
    });
    var tr = "<tr>" +
        "<td>" +
        i +
        "</td>" +
        "<td>" +
        Request_By +
        "</td>" +
        "<td>" +
        Category_Id +
        "</td>" +
        "<td>" +
        Product_id +
        "</td>" +
        "<td>" +
        Product_Details +
        "</td>" +
        "<td>" +
        Quantity +
        "</td>";
    i = i + 1;
    $('#mag_faram_table').find('tbody').last('tr').append(tr);
    //$('#KharidAdeshDetailModel_Quantity').val('');
    clearTextBoxForMagFaram();

}


//
//  Save Mag Faram
//


function saveMagFaramDetails() {
    //alert('lets go...');
    
   
    var magFaramNumber = $('#MagfaramModel_Mag_faram_number').val();
    var RequestByN = $('#MagFaramDetailsModel_Quantity').val();
    var CategoryId = $('#MagFaramDetailsModel_Category_Id').val();
    var ProductIdN = $('#KharidAdeshModel_product_id').val();
    var Quantity = $('#MagFaramDetailsModel_Quantity').val();
    var userId = $('#myHiddenVar').val();
    var OfficeId = $('#officeId').val();
    var Created_Date = $('#nepali_date').val();


    var MagFaram = [];
    MagFaram = {
        Mag_faram_number : magFaramNumber,
        Office_id: OfficeId,
        Created_by: userId,
        Created_date: Created_Date
       
    };

    console.log(magFaramDetails);
     //console.log(MagFaram);



    $.ajax({
        url: MyAppUrlSettings.MagFaramURL,    // _magFaram.cshtml
        type: "POST",
        data: {
            magFaram: MagFaram,
            magFaramDetails: magFaramDetails
        },
        datatype: 'json',
        ContentType: 'application/json;utf-8',
        success: function (resp) {
            $('.flash-message').fadeIn("fast");
            setTimeout(function () {
                $('.flash-message').fadeOut("slow");
            }, 1000)
            //alert('Success ' + resp);
            //console.log(resp)

        },
        error: function (err) {
            alert("Error " + err.status);
        }
    });
    $("#kharid_ades_table tbody tr").remove();
}





/*
 * 
 * Niakasa Start
 * 
 *
 */
function saveNikasa() {
    //alert('lets go nikasa');
    
    var trs = $('#nikasa-table').find('tbody tr:not(:first)');
    var NikasaDetails = [];
    var Store = [];
    var Mag_Faram_Id = $('#NikasaModel_Mag_Faram_Id').val();
    var Office_Id = $('#officeId').val();
    var Created_By = $('#myHiddenVar').val();
    var Created_Date = $('#nepali_date').val();
    var Nikasa;
    Nikasa = {
        Mag_Faram_Id: Mag_Faram_Id,
        Office_Id: Office_Id,
        Created_By: Created_By,
        Created_date: Created_Date
       

    };
    //console.log(Nikasa);
    $.each(trs, function (key, value) {

        var karmachari_id = $(value).find('.karmachari_id').text();
        var category_id = $(value).find('.category_id').text();
        var product_id = $(value).find('.product_id').text();
        var product_details_id = $(value).find('.product_details_id').text();
        var quantity = $(value).find('.quantity').text();
        

        NikasaDetails.push({

            Category_Id: category_id,
            Product_id: product_id,
            Product_Details_Id: product_details_id,
            Quantity: quantity

        });
        Store.push({
            Product_Id: product_id,
            Office_Id: Office_Id,
            Quantity: quantity,
           
           

        });

        
        console.log( Store);

    });

    $.ajax({

        url: MyAppUrlSettings.NikasaDetailsURl,   // _nikasaAddView.cshtml
        type: "POST",
        data: {
            Nikasa: Nikasa,
            NikasaDetails: NikasaDetails,
            Store: Store
        },
        datatype: 'json',
        ContentType: 'application/json;utf-8',
        success: function (resp) {
            $('.flash-message').fadeIn("fast");
            setTimeout(function () {
                $('.flash-message').fadeOut("slow");
            }, 1000)
            //alert('Success ' + resp);
            //console.log(resp)

        },
        error: function (err) {
            alert("Error " + err.status);
        }
    });
}

function clearTextBoxForKharidAdesh() {
    $('#KharidAdeshModel_Category_Id').val("");
    $('#ProductModel_Id').val("");
    $('#ProductDetailsModel_Id').val("");
    $('#KharidAdesDetailsModel_Kharid_type_id').val("");
    $('#KharidAdesDetailsModel_Quantity').val("");


    //$('#btnUpdate').hide();
    //$('#btnAdd').show();
    //$('#name').css('border-color', 'lightgrey');
}

function clearTextBoxForMagFaram() {
    $('#MagFaramDetailsModel_Request_By').val("");
    $('#MagFaramDetailsModel_Category_Id').val("");
    $('#ProductModel_Id').val("");
    $('#ProductDetailsModel_Id').val("");
    $('#MagFaramDetailsModel_Quantity').val("");
}







