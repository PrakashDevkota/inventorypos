//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IMS.Repository.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class saman_barga
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public saman_barga()
        {
            this.mag_kisim = new HashSet<mag_kisim>();
        }
    
        public int id { get; set; }
        public string saman_barga_name { get; set; }
        public Nullable<int> status { get; set; }
        public Nullable<int> created_by { get; set; }
        public string created_date { get; set; }
        public Nullable<int> office_id { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<mag_kisim> mag_kisim { get; set; }
    }
}
